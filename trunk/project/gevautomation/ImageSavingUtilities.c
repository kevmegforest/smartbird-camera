#include "ImageSavingUtilities.h"
#include <string.h>
#include "CameraPicturesSpec.h"
#include <stdio.h>
#include <stdlib.h>

#define cFILEPATH ("/media/photos")

#define TIMEZONE  (-4)

//Save a RGBA image file according to the DNG 1.4 spec.
int SaveRGBAImageFileToDNG(uint32_t format, uint32_t width, uint32_t height, uint32_t depth, uint32_t timestamp_hi,uint32_t timestamp_lo, time_t initial_time, void * image_ptr )
{
    FILE *fp;
    char filename[256];
    uint8_t header[1078];
    uint32_t uint32d;
    uint16_t uint16d;
    
    if (depth !=8) //we only accept 8 bits per pixel
        return 1;
        
    
    // put the uint64_t timestamp
    uint64_t    timestamp;
    timestamp = timestamp_lo + (((uint64_t) timestamp_hi)<<32);
    
    //number of millisecond
    uint32_t ms = (timestamp%1000000)/1000;
    //number of seconds
    uint32_t sec = timestamp/1000000;
    initial_time += sec;
    // Get the time string
    struct tm * time ;
    time = gmtime( &initial_time );
            
//    snprintf(filename , sizeof filename, "%s/%x format - %ux%ux%u size - %uh%um time.dng", cFILEPATH , format, width, height, depth, timestamp_hi, timestamp_lo ); 
    snprintf(filename , sizeof filename, "%s/%.4i_%.2i_%.2i_%.2i_%.2i_%.2i_%.3u_RGBA.dng", cFILEPATH , 1900 + time->tm_year, 1+ time->tm_mon, time->tm_mday, time->tm_hour + TIMEZONE , time->tm_min, time->tm_sec, ms);
    fp = fopen(filename, "w+b");
    if( fp == 0 )
        return -1;
        
        
    //TIFF image header
    header[0]='I';
    header[1]='I';
    uint16d = 42;
    memcpy(&header[2], &uint16d, sizeof(uint16d));
    uint32d = 8;
    memcpy(&header[4], &uint32d, sizeof(uint32d));
    fwrite( &header[0], 1, 8, fp);  
    
    
    uint16d = 14; //Number of directory entries
    memcpy(&header[0], &uint16d, sizeof(uint16d)); 
    
    //compression
    uint16d = 259; //Compression tag
    memcpy(&header[2], &uint16d, sizeof(uint16d));    
    uint16d = 3; //word type
    memcpy(&header[4], &uint16d, sizeof(uint16d));
    uint32d = 1; // count of type
    memcpy(&header[6], &uint32d, sizeof(uint32d));
    uint16d = 1;  //data = nocompression
    memcpy(&header[10], &uint16d, sizeof(uint16d));
    header[12]=0; header[13]=0; //fill the 2 other bytes
    
    //Image length
    uint16d = 257; //tag
    memcpy(&header[14], &uint16d, sizeof(uint16d));
    uint16d = 4; //dword type
    memcpy(&header[16], &uint16d, sizeof(uint16d));
    uint32d = 1; // count of type
    memcpy(&header[18], &uint32d, sizeof(uint32d));
    memcpy(&header[22], &height, sizeof(height));
    
    //Image width
    uint16d = 256; //tag
    memcpy(&header[26], &uint16d, sizeof(uint16d));
    uint16d = 4; //dword type
    memcpy(&header[28], &uint16d, sizeof(uint16d));
    uint32d = 1; // count of type
    memcpy(&header[30], &uint32d, sizeof(uint32d));
    memcpy(&header[34], &width, sizeof(width));
    
    //DNGVersion
    uint16d = 50706; 
    memcpy(&header[38], &uint16d, sizeof(uint16d));
    uint16d = 1; //byte type
    memcpy(&header[40], &uint16d, sizeof(uint16d));
    uint32d = 4 ;
    memcpy(&header[42], &uint32d, sizeof(uint32d));
    header[46] = 1; header[47] = 4; header[48] = 0; header[49] = 0;
    
    //Unique Camera Model
    uint16d = 50708;
    memcpy(&header[50], &uint16d, sizeof(uint16d));
    uint16d= 2; //ascii type
    memcpy(&header[52], &uint16d, sizeof(uint16d));
    uint32d= 4; // 4 character
    memcpy(&header[54], &uint32d, sizeof(uint32d));
    header[58]='G';header[59]='T';header[60]='S';header[61]='\0';
    
    //NewSubFileType
    uint16d = 254; // tag
    memcpy(&header[62], &uint16d, sizeof(uint16d));    
    uint16d = 4; //dword type
    memcpy(&header[64], &uint16d, sizeof(uint16d));
    uint32d = 1; // count of type
    memcpy(&header[66], &uint32d, sizeof(uint32d));
    uint32d = 0;  //data = rawImage
    memcpy(&header[70], &uint32d, sizeof(uint32d)); 
    
    //PhotometricINterpretation
    uint16d = 262; //tag
    memcpy(&header[74], &uint16d, sizeof(uint16d));    
    uint16d = 3; //word type
    memcpy(&header[76], &uint16d, sizeof(uint16d));
    uint32d = 1; // count of type
    memcpy(&header[78], &uint32d, sizeof(uint32d));
    uint16d = 2;  //RGB
    memcpy(&header[82], &uint16d, sizeof(uint16d));
    header[84]=0; header[85]=0;
    
    //PlanarConfiguration
    uint16d = 284; //tag
    memcpy(&header[86], &uint16d, sizeof(uint16d));    
    uint16d = 3; //word type
    memcpy(&header[88], &uint16d, sizeof(uint16d));
    uint32d = 1; // count of type
    memcpy(&header[90], &uint32d, sizeof(uint32d));
    uint16d = 1;  //chunky
    memcpy(&header[94], &uint16d, sizeof(uint16d));
    header[96]=0; header[97]=0;
    
    //add alpha information
    //ExtraSamples
    uint16d = 338; //tag
    memcpy(&header[98], &uint16d, sizeof(uint16d));    
    uint16d = 3; //word type
    memcpy(&header[100], &uint16d, sizeof(uint16d));
    uint32d = 1; // count of type
    memcpy(&header[102], &uint32d, sizeof(uint32d));
    uint16d = 2;  //1 extra sample is alpha channel unassociated
    memcpy(&header[106], &uint16d, sizeof(uint16d));
    header[108]=0; header[109]=0;    
      
    //SamplesPerPixel
    uint16d = 277; //tag
    memcpy(&header[110], &uint16d, sizeof(uint16d));    
    uint16d = 3; //word type
    memcpy(&header[112], &uint16d, sizeof(uint16d));
    uint32d = 1; // count of type
    memcpy(&header[114], &uint32d, sizeof(uint32d));
    uint16d = 4;  // R, G , B and A channel
    memcpy(&header[118], &uint16d, sizeof(uint16d));
    header[120]=0; header[121]=0;
    
    //BitsPerSample
    uint16d = 258; //tag
    memcpy(&header[122], &uint16d, sizeof(uint16d));    
    uint16d = 4; //dword type
    memcpy(&header[124], &uint16d, sizeof(uint16d));
    uint32d = 1; // count of type
    memcpy(&header[126], &uint32d, sizeof(uint32d));
    memcpy(&header[130], &depth, sizeof(depth));
    
    //RowsPerStrip
    uint16d = 278; //tag
    memcpy(&header[134], &uint16d, sizeof(uint16d));    
    uint16d = 4; //dword type
    memcpy(&header[136], &uint16d, sizeof(uint16d));
    uint32d = 1; // count of type
    memcpy(&header[138], &uint32d, sizeof(uint32d));
    uint32d = 1;  //1 strip
    memcpy(&header[142], &uint32d, sizeof(uint32d));
    
    //StripByteCounts
    uint16d = 279; //tag
    memcpy(&header[146], &uint16d, sizeof(uint16d));    
    uint16d = 4; //dword type
    memcpy(&header[148], &uint16d, sizeof(uint16d));
    uint32d = 1; // count of type
    memcpy(&header[150], &uint32d, sizeof(uint32d));
    uint32d = height*width*4;  //the size of the image in bytes
    memcpy(&header[154], &uint32d, sizeof(uint32d));

    //StripOffsets
    uint16d = 273; //tag
    memcpy(&header[158], &uint16d, sizeof(uint16d));    
    uint16d = 4; //word type
    memcpy(&header[160], &uint16d, sizeof(uint16d));
    uint32d = 1; // count of type
    memcpy(&header[162], &uint32d, sizeof(uint32d));
    uint32d = 8+174;  //1 strip address
    memcpy(&header[166], &uint32d, sizeof(uint32d));
    
    //End of Directory entry
    uint32d=0;
    memcpy(&header[170], &uint32d, sizeof(uint32d));
    
    //write
    fwrite( &header[0], 1, 174, fp);
    fwrite( image_ptr , width*height*4,1,  fp);
    fclose(fp);
    
    return 0; 
}

//Save a file according to the DNG 1.4 spec. This is for raw pictures
int SaveImageFileToDNG(uint32_t format, uint32_t width, uint32_t height, uint32_t depth, uint32_t f_hour, uint32_t f_min, uint32_t f_sec, uint32_t f_msec  , void * image_ptr )
{
    FILE *fp;
    char filename[256];
    uint8_t header[1078];
    uint32_t uint32d;
    uint16_t uint16d;
    
    if (depth !=8) //we only accept 8 bits per pixel
        return 1;
            
//    snprintf(filename , sizeof filename, "%s/%x format - %ux%ux%u size - %uh%um time.dng", cFILEPATH , format, width, height, depth, timestamp_hi, timestamp_lo ); 
    snprintf(filename , sizeof filename, "%s/%.2i_%.2i_%.2i_%.3u.dng", cFILEPATH, f_hour , f_min, f_sec, f_msec);
    fp = fopen(filename, "w+b");
    if( fp == 0 )
        return -1;
        
        
    //TIFF image header
    header[0]='I';
    header[1]='I';
    uint16d = 42;
    memcpy(&header[2], &uint16d, sizeof(uint16d));
    uint32d = 8;
    memcpy(&header[4], &uint32d, sizeof(uint32d));
    fwrite( &header[0], 1, 8, fp);  
    
    
    uint16d = 15; //Number of directory entries
    memcpy(&header[0], &uint16d, sizeof(uint16d)); 
    
    //compression
    uint16d = 259; //Compression tag
    memcpy(&header[2], &uint16d, sizeof(uint16d));    
    uint16d = 3; //word type
    memcpy(&header[4], &uint16d, sizeof(uint16d));
    uint32d = 1; // count of type
    memcpy(&header[6], &uint32d, sizeof(uint32d));
    uint16d = 1;  //data = nocompression
    memcpy(&header[10], &uint16d, sizeof(uint16d));
    header[12]=0; header[13]=0; //fill the 2 other bytes
    
    //Image length
    uint16d = 257; //tag
    memcpy(&header[14], &uint16d, sizeof(uint16d));
    uint16d = 4; //dword type
    memcpy(&header[16], &uint16d, sizeof(uint16d));
    uint32d = 1; // count of type
    memcpy(&header[18], &uint32d, sizeof(uint32d));
    memcpy(&header[22], &height, sizeof(height));
    
    //Image width
    uint16d = 256; //tag
    memcpy(&header[26], &uint16d, sizeof(uint16d));
    uint16d = 4; //dword type
    memcpy(&header[28], &uint16d, sizeof(uint16d));
    uint32d = 1; // count of type
    memcpy(&header[30], &uint32d, sizeof(uint32d));
    memcpy(&header[34], &width, sizeof(width));
    
    //DNGVersion
    uint16d = 50706; 
    memcpy(&header[38], &uint16d, sizeof(uint16d));
    uint16d = 1; //byte type
    memcpy(&header[40], &uint16d, sizeof(uint16d));
    uint32d = 4 ;
    memcpy(&header[42], &uint32d, sizeof(uint32d));
    header[46] = 1; header[47] = 4; header[48] = 0; header[49] = 0;
    
    //Unique Camera Model
    uint16d = 50708;
    memcpy(&header[50], &uint16d, sizeof(uint16d));
    uint16d= 2; //ascii type
    memcpy(&header[52], &uint16d, sizeof(uint16d));
    uint32d= 4; // 4 character
    memcpy(&header[54], &uint32d, sizeof(uint32d));
    header[58]='G';header[59]='T';header[60]='S';header[61]='\0';
    
    //NewSubFileType
    uint16d = 254; // tag
    memcpy(&header[62], &uint16d, sizeof(uint16d));    
    uint16d = 4; //dword type
    memcpy(&header[64], &uint16d, sizeof(uint16d));
    uint32d = 1; // count of type
    memcpy(&header[66], &uint32d, sizeof(uint32d));
    uint32d = 0;  //data = rawImage
    memcpy(&header[70], &uint32d, sizeof(uint32d)); 
    
    //PhotometricINterpretation
    uint16d = 262; //tag
    memcpy(&header[74], &uint16d, sizeof(uint16d));    
    uint16d = 3; //word type
    memcpy(&header[76], &uint16d, sizeof(uint16d));
    uint32d = 1; // count of type
    memcpy(&header[78], &uint32d, sizeof(uint32d));
    uint16d = 32803;  //CFA_Bayer
    memcpy(&header[82], &uint16d, sizeof(uint16d));
    header[84]=0; header[85]=0;
    
    //PlanarConfiguration
    uint16d = 284; //tag
    memcpy(&header[86], &uint16d, sizeof(uint16d));    
    uint16d = 3; //word type
    memcpy(&header[88], &uint16d, sizeof(uint16d));
    uint32d = 1; // count of type
    memcpy(&header[90], &uint32d, sizeof(uint32d));
    uint16d = 1;  //chunky
    memcpy(&header[94], &uint16d, sizeof(uint16d));
    header[96]=0; header[97]=0;
    
    //CFARepeatPatternDim
    uint16d = 33421; //tag
    memcpy(&header[98], &uint16d, sizeof(uint16d));    
    uint16d = 3; //word type
    memcpy(&header[100], &uint16d, sizeof(uint16d));
    uint32d = 2; // count of type
    memcpy(&header[102], &uint32d, sizeof(uint32d));
    uint16d = 2;  
    memcpy(&header[106], &uint16d, sizeof(uint16d)); 
    memcpy(&header[108], &uint16d, sizeof(uint16d));
    
    //CFAPattern
    uint16d = 33422; //tag
    memcpy(&header[110], &uint16d, sizeof(uint16d));    
    uint16d = 1; //byte type
    memcpy(&header[112], &uint16d, sizeof(uint16d));
    uint32d = 4; // The Bayer Grid Array description
    memcpy(&header[114], &uint32d, sizeof(uint32d));
    header[118]=1; header[119]=2;
    header[120]=0; header[121]=1;    
    //R=0; G=1 ; B=2;
      
    //SamplesPerPixel
    uint16d = 277; //tag
    memcpy(&header[122], &uint16d, sizeof(uint16d));    
    uint16d = 3; //word type
    memcpy(&header[124], &uint16d, sizeof(uint16d));
    uint32d = 1; // count of type
    memcpy(&header[126], &uint32d, sizeof(uint32d));
    uint16d = 1;  
    memcpy(&header[130], &uint16d, sizeof(uint16d));
    header[132]=0; header[133]=0;
    
    //BitsPerSample
    uint16d = 258; //tag
    memcpy(&header[134], &uint16d, sizeof(uint16d));    
    uint16d = 4; //dword type
    memcpy(&header[136], &uint16d, sizeof(uint16d));
    uint32d = 1; // count of type
    memcpy(&header[138], &uint32d, sizeof(uint32d));
    memcpy(&header[142], &depth, sizeof(depth));
    
    //RowsPerStrip
    uint16d = 278; //tag
    memcpy(&header[146], &uint16d, sizeof(uint16d));    
    uint16d = 4; //dword type
    memcpy(&header[148], &uint16d, sizeof(uint16d));
    uint32d = 1; // count of type
    memcpy(&header[150], &uint32d, sizeof(uint32d));
    uint32d = 1;  //1 strip
    memcpy(&header[154], &uint32d, sizeof(uint32d));
    
    //StripByteCounts
    uint16d = 279; //tag
    memcpy(&header[158], &uint16d, sizeof(uint16d));    
    uint16d = 4; //dword type
    memcpy(&header[160], &uint16d, sizeof(uint16d));
    uint32d = 1; // count of type
    memcpy(&header[162], &uint32d, sizeof(uint32d));
    uint32d = height*width;  //the size of the image in bytes
    memcpy(&header[166], &uint32d, sizeof(uint32d));

    //StripOffsets
    uint16d = 273; //tag
    memcpy(&header[170], &uint16d, sizeof(uint16d));    
    uint16d = 4; //word type
    memcpy(&header[172], &uint16d, sizeof(uint16d));
    uint32d = 1; // count of type
    memcpy(&header[174], &uint32d, sizeof(uint32d));
    uint32d = 8+186;  //1 strip address
    memcpy(&header[178], &uint32d, sizeof(uint32d));
    
    //End of Directory entry
    uint32d=0;
    memcpy(&header[182], &uint32d, sizeof(uint32d));
    
    //write
    fwrite( &header[0], 1, 186, fp);
    fwrite( image_ptr , width*height,1,  fp);
    fclose(fp);
    
    //save a demosaiced picture with the dcraw program
   // char cmdline[256];
    //snprintf(cmdline, sizeof(cmdline), "dcraw -r 1.0 1.0 1.0 1.0 -q 3 -c  %s | ppmtobmp > %s/%.4i_%.2i_%.2i_%.2i_%.2i_%.2i_%.3u.BMP" , filename , cFILEPATH , 1900 + time->tm_year, time->tm_mon, time->tm_mday, time->tm_hour, time->tm_min, time->tm_sec, ms);
  //  snprintf(cmdline, sizeof(cmdline), "dcraw -r 1.0 1.0 1.0 1.0 -q 0  %s " , filename );
   // snprintf(cmdline, sizeof(cmdline), "dcraw -r 1.0 1.0 1.0 1.0 -q 0 -c  %s | pnmtopng -compression=0 > %s/%.4i_%.2i_%.2i_%.2i_%.2i_%.2i_%.3u.PNG" , filename , cFILEPATH , 1900 + time->tm_year, time->tm_mon+ 1, time->tm_mday, time->tm_hour + TIMEZONE, time->tm_min, time->tm_sec, ms);
   // system(cmdline);
    return 0; 
}

int SaveImageFileToBitMap( uint32_t format, uint32_t width, uint32_t height, uint8_t depth, uint32_t timestamp_hi,uint32_t timestamp_lo, void * image_ptr )
{
//http://www.fastgraph.com/help/bmp_header_format.html
//bitmap file windows format
    FILE *fp;
    char filename[256];
    uint8_t header[1078];
    uint32_t uint32d;
    uint16_t uint16d;
    
    if (depth !=8) //we only accept 8 bits per pixel
        return 1;
    //signature
    uint16d = 0x4D42;
    memcpy(&header[0], &uint16d, sizeof(uint16d));
    //size of file
    uint32d = 1078 + width*height;
    memcpy(&header[2], &uint32d, sizeof(uint32d));
    //reserved
    header[6]= 0;
    header[7]= 0;
    header[8]= 0;
    header[9]= 0;
    //offset to start of image
    uint32d = 1078;
    memcpy(&header[10], &uint32d, sizeof(uint32d));
    //size of bitmapcoreheader, 12 bytes
    uint32d = 40 ;
    memcpy(&header[14], &uint32d,sizeof(uint32d));
    //width
    memcpy(&header[18], &width, sizeof(width));
    //height
    memcpy(&header[22], &height, sizeof(height));
    //number of planes in the image, must be 1
    uint16d = 1;
    memcpy(&header[26], &uint16d, sizeof(uint16d));
    //bits per pixel
    uint16d = 8;
    memcpy(&header[28], &uint16d, sizeof(uint16d));
    //compression = none
    header[30]= 0;
    header[31]= 0;
    header[32]= 0;
    header[33]= 0;
    //size of image data (including padding)
    uint32d = width*height;
    memcpy( &header[34], &uint32d, sizeof(uint32d));
    //zero for everything else
    for ( uint16d=38; uint16d< 54; uint16d++ )
        header[uint16d]= 0;
    //color table
    for( uint16d = 0 ; uint16d<256 ; uint16d++)
    {
        header[54+uint16d*4] = (uint8_t) uint16d;
        header[55+uint16d*4] = (uint8_t) uint16d;
        header[56+uint16d*4] = (uint8_t) uint16d;
        header[57+uint16d*4] = 0;
    }
    
    snprintf(filename , sizeof filename, "%s/%x format - %ux%ux%u size - %uh%um time.bmp", cFILEPATH , format, width, height, depth, timestamp_hi, timestamp_lo ); 
    fp = fopen(filename, "w+b");
    if( fp == 0 )
        return -1;
    fwrite( &header[0], 1, 1078, fp);
    fwrite( image_ptr , width*height,1,  fp);
    return fclose(fp);
}


int SaveJPEGImageFile(uint32_t format, uint32_t width, uint32_t height, uint32_t depth, uint32_t f_hour, uint32_t f_min, uint32_t f_sec, uint32_t f_msec  , void * image_ptr, uint32_t f_fileSize )
{
    FILE *fp;
    char filename[256];

    if (depth !=8) //we only accept 8 bits per pixel
        return 1;


//    snprintf(filename , sizeof filename, "%s/%x format - %ux%ux%u size - %uh%um time.dng", cFILEPATH , format, width, height, depth, timestamp_hi, timestamp_lo );
    snprintf(filename , sizeof filename, "%s/%.2i_%.2i_%.2i_%.3u.jpg", cFILEPATH, f_hour , f_min, f_sec, f_msec);
    fp = fopen(filename, "w+b");
    if( fp == 0 )
        return -1;

    //write
    fwrite( image_ptr , f_fileSize ,1,  fp);
    fclose(fp);

    return 0;
}
