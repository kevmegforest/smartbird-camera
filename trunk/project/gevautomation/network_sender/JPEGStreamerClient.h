/*
 * JPEGStreamerClient.h
 *
 *  Created on: Jun 14, 2014
 *      Author: kevmegforest
 */

#ifndef JPEGSTREAMERCLIENT_H_
#define JPEGSTREAMERCLIENT_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

void netStreamer_initialize();
void netStreamer_cleanup();

void netStreamer_addPictureToSend(void * f_pictureData, uint32_t f_pictureSize , uint8_t f_hour, uint8_t f_minute, uint8_t f_second, uint16_t f_millisecond);

//typedef struct netStreamerPacket;
//void netStreamer_addPictureToSendWithoutCopy(netStreamerPacket* f_packetData, uint32_t f_pictureSize , uint8_t f_hour, uint8_t f_minute, uint8_t f_second, uint16_t f_millisecond);


#ifdef __cplusplus
}
#endif



#endif /* JPEGSTREAMERCLIENT_H_ */
