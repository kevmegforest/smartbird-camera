#include <unistd.h>
#include <cstdlib>
#include <cstring>
#include <netdb.h>
#include <iostream>
#include "udt4/src/udt.h"
#include <memory>
#include "JPEGStreamerClient.h"

#include "pthread.h"

using namespace std;
//flow typique -> initialize(), sendImage(), sendImage(), ..., cleanup();

typedef struct {
	uint8_t hour;
	uint8_t minute;
	uint8_t second;
	uint8_t padding0;
	uint16_t millisecond;
	uint16_t padding1;
	uint32_t imageSize;
} theader;

static pthread_t threadId;
typedef enum
{
	RUN,
	STOP,
	FINISH
} tThreadStatus ;
static tThreadStatus threadStatus = FINISH;
static pthread_mutex_t mutexThreadStatus ;

int netStreamer_sendPacket(void * f_packetData)
{
	//get size of data to send
	uint32_t packetSize = (*((theader*)(f_packetData))).imageSize + sizeof(theader);
    struct addrinfo hints, *local, *peer;

    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_flags = AI_PASSIVE;
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;

    if (0 != getaddrinfo(NULL, "9000", &hints, &local)) //J'utilise le port 9000 finalement
    {
	   cout << "incorrect network address.\n" << endl;
	   return -1;
    }

    UDTSOCKET client = UDT::socket(local->ai_family, local->ai_socktype, local->ai_protocol);
    freeaddrinfo(local);

    if (0 != getaddrinfo("192.168.1.152" /*TODO: setter adresse ip statique de la base au sol*/, "9000", &hints, &peer))
    {
 	  cout << "incorrect server/peer address. " << endl;
 	  return -1;
    }

    // connect to the server, implicit bind
    if (UDT::ERROR == UDT::connect(client, peer->ai_addr, peer->ai_addrlen))
    {
    	cout << "connect: " << UDT::getlasterror().getErrorMessage() << endl;
    	return -1;
    }

    freeaddrinfo(peer);

	uint32_t ssize = 0;
	int ss;
	//send data
	while (ssize < packetSize)
	{
		if (UDT::ERROR == (ss = UDT::send(client, ((const char *) f_packetData) + ssize, packetSize - ssize, 0)))
		{
			std::cout << "send: FATAL ERROR " << UDT::getlasterror().getErrorMessage() << std::endl;
			return -1;
		}
		//std::cout << "Sent " << ss << " B" << std::endl;
		ssize += ss;
	}
	UDT::close(client);
	return 0;
}

static uint32_t stack_head = 0, stack_tail = 0;
static pthread_mutex_t mutexStackHead;
static pthread_mutex_t mutexStackTail;
static pthread_mutex_t mutexAddPicture;

//have a stack of 64 pictures
#define cJPEGSTREAMER_CLIENT_STACK_SIZE 64
static void * stack_packet[cJPEGSTREAMER_CLIENT_STACK_SIZE] = {0};


void * netStreamer_thread(void * threadStatus )
{
	//initialize stack
	pthread_mutex_lock(&mutexStackHead);
	pthread_mutex_lock (&mutexStackTail);
	stack_head = 0;
	stack_tail = 0;
	pthread_mutex_unlock (&mutexStackHead);
	pthread_mutex_unlock (&mutexStackTail);


	pthread_mutex_lock (&mutexThreadStatus);
	while( *( tThreadStatus * )threadStatus == RUN )
	{
		pthread_mutex_unlock(&mutexThreadStatus);

		pthread_mutex_lock(&mutexStackHead);
		pthread_mutex_lock(&mutexStackTail);
		if( stack_head != stack_tail )
		{
			//printf("\nnT_sendPacket...%p", (stack_packet[stack_head]) );
			pthread_mutex_unlock(&mutexStackTail);
			//get pointer to packetData
			void * packetData = (stack_packet[stack_head]);
			pthread_mutex_unlock(&mutexStackHead);


			//retry until it works
			if( netStreamer_sendPacket(packetData) )
			{

			}
			else
			{

				//free memory
				free(packetData);

				pthread_mutex_lock(&mutexStackHead);
				stack_packet[stack_head] = NULL;

				//increase head
				stack_head = (stack_head + 1)%cJPEGSTREAMER_CLIENT_STACK_SIZE;
				pthread_mutex_unlock (&mutexStackHead);
			}

		}
		else
		{
			pthread_mutex_unlock (&mutexStackHead);
			pthread_mutex_unlock (&mutexStackTail);
		}
		usleep(1000);
		pthread_mutex_lock (&mutexThreadStatus);
	}
	*( tThreadStatus * )threadStatus = FINISH;
	pthread_mutex_unlock (&mutexThreadStatus);

	pthread_exit(NULL);
}

void netStreamer_addPictureToSend(void * f_pictureData, uint32_t f_pictureSize , uint8_t f_hour, uint8_t f_minute, uint8_t f_second, uint16_t f_millisecond)
{
	pthread_mutex_lock(&mutexAddPicture);

	//printf("\naPS_beginPictureAdd");
	pthread_mutex_lock(&mutexStackTail);
	pthread_mutex_lock(&mutexStackHead);
	if((stack_tail + 1)%cJPEGSTREAMER_CLIENT_STACK_SIZE == stack_head)
	{
		cerr<<"\n*** [ERROR] NetStreamerPicture Stack is Full : \n"
				<< "Freeing memory to send more photos! \n";
		stack_tail = (stack_head + 2)%cJPEGSTREAMER_CLIENT_STACK_SIZE;
		int end_pos = stack_head;
		int pos = stack_tail;
		while (end_pos != pos)
		{
			free(stack_packet[pos]);
			pos = (pos+1)%cJPEGSTREAMER_CLIENT_STACK_SIZE;
		}
	}
	pthread_mutex_unlock(&mutexStackTail);
	pthread_mutex_unlock(&mutexStackHead);
	//printf("\naPS_1");
	//initialize a memory space with the needed size for the packet to send
	void * pPacket = malloc(f_pictureSize + sizeof(theader));
	//printf("\naPS_pointer = %p", pPacket);
	//Create header
	*(theader*) (pPacket) =(theader){ f_hour, f_minute, f_second, 0 , f_millisecond, 0, f_pictureSize };
	//memcopy the picture
	memcpy( ((char*)pPacket) + sizeof(theader),f_pictureData,f_pictureSize );
	//add pointer to stack
	pthread_mutex_lock(&mutexStackTail);
	stack_packet[stack_tail] = pPacket;
	//increase stack_tail
	stack_tail = (stack_tail + 1)%cJPEGSTREAMER_CLIENT_STACK_SIZE;
	pthread_mutex_unlock(&mutexStackTail);

	//printf("\naPS_end");

	pthread_mutex_unlock(&mutexAddPicture);

	pthread_mutex_unlock(&mutexAddPicture);
}

typedef struct{
	theader header;
	char pictureData[0];
} netStreamerPacket;

//Be sure to send a pointer where the pictureData is in the packet Struct
void netStreamer_addPictureToSendWithoutCopy(netStreamerPacket* f_packetData, uint32_t f_pictureSize , uint8_t f_hour, uint8_t f_minute, uint8_t f_second, uint16_t f_millisecond)
{
	pthread_mutex_lock(&mutexAddPicture);

	pthread_mutex_lock(&mutexStackTail);
	pthread_mutex_lock(&mutexStackHead);
	if((stack_tail + 1)%cJPEGSTREAMER_CLIENT_STACK_SIZE == stack_head)
	{
		cerr<<"\n*** [ERROR] NetStreamerPicture Stack is Full : \n"
				<< "Freeing memory to send more photos! \n";
		stack_tail = (stack_head + 2)%cJPEGSTREAMER_CLIENT_STACK_SIZE;
		int end_pos = stack_head;
		int pos = stack_tail;
		while (end_pos != pos)
		{
			free(stack_packet[pos]);
			pos = (pos+1)%cJPEGSTREAMER_CLIENT_STACK_SIZE;
		}
	}
	//Create header
	f_packetData->header =(theader){ f_hour, f_minute, f_second, 0 , f_millisecond, 0, f_pictureSize };
	stack_packet[stack_tail] = (f_packetData);
	//increase stack_tail
	stack_tail = (stack_tail + 1)%cJPEGSTREAMER_CLIENT_STACK_SIZE;

	pthread_mutex_unlock(&mutexStackTail);
	pthread_mutex_unlock(&mutexStackHead);

	pthread_mutex_unlock(&mutexAddPicture);

}

void netStreamer_initialize()
{
	UDT::startup();
	pthread_mutex_init(&mutexStackHead, NULL);
	pthread_mutex_init(&mutexStackTail, NULL);
	pthread_mutex_init(&mutexThreadStatus, NULL);
	pthread_mutex_init(&mutexAddPicture, NULL);

	//create thread that will contains image pointer and then send those pictures
	pthread_mutex_lock(&mutexThreadStatus);
	if(threadStatus == FINISH)
	{
		threadStatus = RUN;
		pthread_create(&threadId,NULL, netStreamer_thread , (void *) &threadStatus);
	}
	pthread_mutex_unlock(&mutexThreadStatus);
}

void netStreamer_cleanup()
{
	usleep(100000);
	printf("\nCleanup \n");
	pthread_mutex_lock(&mutexThreadStatus);
	threadStatus=STOP;
	while(threadStatus!=FINISH)
	{
		pthread_mutex_unlock(&mutexThreadStatus);
		usleep(10000);
		pthread_mutex_lock(&mutexThreadStatus);
	}
	pthread_mutex_unlock(&mutexThreadStatus);

	pthread_mutex_destroy(&mutexStackHead);
	pthread_mutex_destroy(&mutexStackTail);
	pthread_mutex_destroy(&mutexThreadStatus);
	pthread_mutex_destroy(&mutexAddPicture);
	UDT::cleanup();
}

