#include <stdio.h>
#include <stdlib.h>
#include "../bayer_demosaicing/util.h"
#include "JPEGStreamerClient.h"

#include "gl_compatibility_3_0.h"

int main(void)
{
	//Load a dng picture to get it's raw Bayer data
	char filename[256] = "../bayer_demosaicing/testPicture/testInputData.dng";
	int length;
	void * fileData = file_contents( filename, (GLint *) &length);
	if (fileData == 0 || length == 0 )
		return -1;

	//initialise Network streamer
	netStreamer_initialize();

	//send picture
	netStreamer_sendImage(fileData, (uint32_t) length, 23,24,25,26 );

	//Cleanup
	netStreamer_cleanup();

	free(fileData);
	return 0;

}
