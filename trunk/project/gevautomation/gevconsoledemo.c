#include "stdio.h"
#include "cordef.h"
#include "gevapi.h"				//!< GEV lib definitions.
#include "SapX11Util.h"
#include <sched.h>
#include <stdint.h>
#include <string.h>
#include "ImageSavingUtilities.h"

#include <GL/freeglut.h>

#include <time.h>

#include "main.h"

#define MAX_NETIF		8
#define MAX_CAMERAS_PER_NETIF	32
#define MAX_CAMERAS		(MAX_NETIF * MAX_CAMERAS_PER_NETIF)

// Enable/disable transfer tuning (buffering, timeouts, thread affinity).
#define TUNE_STREAMING_THREADS 1


#define NUM_BUF	8

#define cFRAMERATE (8000) // in mHz
#define cPICTURE_SAVING_FREQUENCY (500) //in mHz

#include "CameraPicturesSpec.h" //Load default picture configuration

#include "bayer_demosaicing/bayer_jpeg_and_save.h"

typedef struct tagMY_CONTEXT
{
	GEV_CAMERA_HANDLE camHandle;
	int		depth;
	int             height;
	int             width;
	int 		format;
	time_t          initial_time;
	BOOL            exit;
}MY_CONTEXT, *PMY_CONTEXT;

static unsigned long us_timer_init( void )
{
   struct timeval tm;
   unsigned long msec;
   
   // Get the time and turn it into a millisecond counter.
   gettimeofday( &tm, NULL);
   
   msec = (tm.tv_sec * 1000000) + (tm.tv_usec);
   return msec;
}
static unsigned long ms_timer_init( void )
{
   struct timeval tm;
   unsigned long msec;
   
   // Get the time and turn it into a millisecond counter.
   gettimeofday( &tm, NULL);
   
   msec = (tm.tv_sec * 1000) + (tm.tv_usec / 1000);
   return msec;
}

static int ms_timer_interval_elapsed( unsigned long origin, unsigned long timeout)
{
   struct timeval tm;
   unsigned long msec;
   
   // Get the time and turn it into a millisecond counter.
   gettimeofday( &tm, NULL);
   
   msec = (tm.tv_sec * 1000) + (tm.tv_usec / 1000);
      
   // Check if the timeout has expired.
   if ( msec > origin )
   {
      return ((msec - origin) >= timeout) ? TRUE : FALSE;
   }
   else
   {
      return ((origin - msec) >= timeout) ? TRUE : FALSE;
   }
}


char GetKey()
{
   char key = getchar();
   while ((key == '\r') || (key == '\n'))
   {
      key = getchar();
   }
   return key;
}

void PrintMenu()
{
   printf("Control       : [Q]or[ESC]=end\n");
}


void * ImageSavingThread( void *context)
{
	MY_CONTEXT *imcontext = (MY_CONTEXT *)context;

    if( imcontext != NULL)
    {
        unsigned int picSave = 0;    
        // While we are still running.
	    while(!imcontext->exit)
	    {
		    GEV_BUFFER_OBJECT *img = NULL;
		    GEV_STATUS status = 0;

		    // Wait for images to be received
		    status = GevWaitForNextImage(imcontext->camHandle, &img, 1000);

		    if ((img != NULL) && (status == GEVLIB_OK))
		    {
			    if (img->status == 0)
			    {
			        //We dont save every picture we only have 40 GB
				    if(picSave == cFRAMERATE/cPICTURE_SAVING_FREQUENCY )
                    {
				        // put the uint64_t timestamp
				        uint64_t    timestamp;
				        timestamp = img->timestamp_lo + (((uint64_t) img->timestamp_hi)<<32);

				        //number of millisecond
				        uint32_t ms = (timestamp%1000000)/1000;
				        //number of seconds
				        uint32_t sec = timestamp/1000000;
				        time_t initial_time =  imcontext->initial_time + sec;
				        // Get the time string
				        struct tm * time ;
				        time = gmtime( &initial_time );

				    	bayerJpegSave_addRawBayerPicture(img->address,time->tm_hour,time->tm_min,time->tm_sec, ms);
				        picSave=0;
				    }
				    else
				        picSave++;
				    //printf("%u \n",  img->d);
				    
				    
#if 0
                uint32_t read;
                GEV_REGISTER camreg ;
                
                //debug by reading all the settings set if they are good
                GevGetRegisterByName((imcontext->camHandle), "AcquisitionFrameRateRaw", &camreg);
				//GevRegisterWriteInt(handle, &camreg, 0, cFRAMERATE) ; //8 Hz
				GevRegisterReadInt((imcontext->camHandle),&camreg, 0, &read);
				GevGetRegisterByName((imcontext->camHandle), "exposureDelay", &camreg);
				//GevRegisterWriteInt(handle, &camreg, 0, 0) ; //0us
				GevRegisterReadInt((imcontext->camHandle),&camreg, 0, &read);
				printf("ExposureDelay : %u \n", read);
				GevGetRegisterByName((imcontext->camHandle), "ExposureTimeRaw", &camreg);
				//GevRegisterWriteInt(handle, &camreg, 0, 1000) ; //10000us
				GevRegisterReadInt((imcontext->camHandle),&camreg, 0, &read);
#define EXPOSURE_AUTO_MAX_VALUE_REG_ADDRESS (0x200056dc)
 //   			Gev_WriteReg ((imcontext->camHandle), EXPOSURE_AUTO_MAX_VALUE_REG_ADDRESS , __bswap_32( 100000) ); //100000 us
#define EXPOSURE_AUTO_MIN_VALUE_REG_ADDRESS (0x2000598c)    			
 //   			Gev_WriteReg ((imcontext->camHandle), EXPOSURE_AUTO_MIN_VALUE_REG_ADDRESS , __bswap_32( 500 )); //500 us
    			printf("ExposureTimeRaw : %u \n", read);
    			Gev_ReadReg ((imcontext->camHandle), EXPOSURE_AUTO_MIN_VALUE_REG_ADDRESS, &read);
    			printf("ExposureAutoMinValue : %u \n", read);
    			Gev_ReadReg ((imcontext->camHandle), EXPOSURE_AUTO_MAX_VALUE_REG_ADDRESS, &read);
    			printf("ExposureAutoMaxValue : %u \n", read);
    			//This doesnt work
    			//GevGetRegisterByName(handle, "exposureAutoMaxValue", &camreg);
				//GevRegisterWriteInt(handle, &camreg, 0, 100000) ; //100000 us
				//GevRegisterReadInt(handle,&camreg, 0, &read);
				//GevGetRegisterByName(handle, "exposureAutoMinValue", &camreg);
				//GevRegisterWriteInt(handle, &camreg, 0, 500) ; //500 us
				//GevRegisterReadInt(handle,&camreg, 0, &read);
				
				GevGetRegisterByName((imcontext->camHandle), "ExposureMode", &camreg);
				//GevRegisterWriteInt(handle, &camreg, 0, 0) ; //Timed
				GevRegisterReadInt((imcontext->camHandle),&camreg, 0, &read);
				GevGetRegisterByName((imcontext->camHandle), "exposureAlignment", &camreg);
				//GevRegisterWriteInt(handle, &camreg, 0, 0) ; //Synchronous
				GevRegisterReadInt((imcontext->camHandle),&camreg, 0, &read);
				GevGetRegisterByName((imcontext->camHandle), "ExposureAuto", &camreg);
				//GevRegisterWriteInt(handle, &camreg, 0, 0 ) ; //Continuous
				GevRegisterReadInt((imcontext->camHandle),&camreg, 0, &read);
				GevGetRegisterByName((imcontext->camHandle), "GainSelector", &camreg);
				//GevRegisterWriteInt(handle, &camreg, 0, 1) ; //AnalogAll
				GevRegisterReadInt((imcontext->camHandle),&camreg, 0, &read);
				GevGetRegisterByName((imcontext->camHandle), "GainRaw", &camreg);
				//GevRegisterWriteInt(handle, &camreg, 0, 0); //1
				GevRegisterReadInt((imcontext->camHandle),&camreg, 0, &read);
				printf("GainRaw : %u \n", read);
				GevGetRegisterByName((imcontext->camHandle), "GainAuto", &camreg);
				//GevRegisterWriteInt(handle, &camreg, 0, 2) ; //Continuous
				GevRegisterReadInt((imcontext->camHandle),&camreg, 0, &read);
				GevGetRegisterByName((imcontext->camHandle), "gainAutoMaxValueRaw", &camreg);
				//GevRegisterWriteInt(handle, &camreg, 0, (4-1)*512) ; //4
				GevRegisterReadInt((imcontext->camHandle),&camreg, 0, &read);
				GevGetRegisterByName((imcontext->camHandle), "gainAutoMinValueRaw", &camreg);
				//GevRegisterWriteInt(handle, &camreg, 0, 0) ; //1
				GevRegisterReadInt((imcontext->camHandle),&camreg, 0, &read);
				
#endif
			    }
			    else
			    {
				    // Image had an error (incomplete (timeout/overflow/lost)).
				    // Do any handling of this condition necessary.
			    }
		    }

	    }
    }
	pthread_exit(0);	
}


int g_main_argc = 0;
char** g_main_argv = NULL;

int main(int argc, char* argv[])
{
	g_main_argc = argc;
	g_main_argv = argv;



	GEV_DEVICE_INTERFACE  pCamera[MAX_CAMERAS] = {0};
	UINT16 status;
	int numCamera = 0;
	int camIndex = 0;
	MY_CONTEXT context = {0};
    pthread_t  tid;
	char c;
	int done = FALSE;

	// Greetings
	printf ("\nGigE Vision Library SmartBird Capture Program (%s)\n", __DATE__);
	
	//initialise OpenGL
	

	//===================================================================================
	// Set default options for the library.
	{
		GEVLIB_CONFIG_OPTIONS options = {0};

		GevGetLibraryConfigOptions( &options);
		//options.logLevel = GEV_LOG_LEVEL_OFF;
		//options.logLevel = GEV_LOG_LEVEL_TRACE;
		options.logLevel = GEV_LOG_LEVEL_NORMAL;
		GevSetLibraryConfigOptions( &options);
	}

	//====================================================================================
	// DISCOVER Cameras
	//
	// Get all the IP addresses of attached network cards.

    while (numCamera == 0)
	{
	    status = GevGetCameraList( pCamera, MAX_CAMERAS, &numCamera);
    }
    
	printf ("%d camera(s) on the network\n", numCamera);


	// Select the first camera found (unless the command line has a parameter = the camera index)	
	{
		if (argc > 1)
		{
			sscanf(argv[1], "%d", &camIndex);
			if (camIndex >= (int)numCamera)
			{
				printf("Camera index out of range - only %d camera(s) are present\n", numCamera);
				camIndex = -1;
			}
		}

		if (camIndex != -1)
		{
			//====================================================================
			// Connect to Camera
			//
			//
			int i;
			UINT32 height = 0;
			UINT32 width = 0;
			UINT32 x_offset = 0;
			UINT32 y_offset = 0;
			UINT32 format = 0;
			UINT32 maxHeight = 3072;
			UINT32 maxWidth = 4096;
			UINT32 maxDepth = 2;
			UINT32 size;
			int numBuffers = NUM_BUF;
			PUINT8 bufAddress[NUM_BUF];
			GEV_CAMERA_HANDLE handle = NULL;
			UINT32 pixFormat = 0;
			UINT32 pixDepth = 0;
			UINT32 pixelOrder = 0;
            UINT32 frameRate = 1;
			// Open the camera.
			status = GevOpenCamera( &pCamera[0], GevExclusiveMode, &handle);
			if (status == 0)
			{
				DALSA_GENICAM_GIGE_REGS reg = {0};
				GEV_CAMERA_OPTIONS camOptions = {0};

				// Adjust the camera interface options if desired (see the manual)
				GevGetCameraInterfaceOptions( handle, &camOptions);
				camOptions.heartbeat_timeout_ms = 90000;		// For debugging (delay camera timeout while in debugger)

#if TUNE_STREAMING_THREADS
				// Some tuning can be done here. (see the manual)
				camOptions.streamFrame_timeout_ms = 1001;				// Internal timeout for frame reception.
				camOptions.streamNumFramesBuffered = 4;				// Buffer frames internally.
				camOptions.streamMemoryLimitMax = 64*1024*1024;		// Adjust packet memory buffering limit.	
				camOptions.streamPktSize = 3968;							// Adjust the GVSP packet size.
				camOptions.streamPktDelay = 10;							// Add usecs between packets to pace arrival at NIC.
				
				// Assign specific CPUs to threads (affinity) - if required for better performance.
				{
					int numCpus = _GetNumCpus();
					if (numCpus > 1)
					{
						camOptions.streamThreadAffinity = numCpus-1;
						camOptions.serverThreadAffinity = numCpus-2;
					}
				}
#endif
				// Write the adjusted interface options back.
				GevSetCameraInterfaceOptions( handle, &camOptions);

				//=================================================================
				// Get the camera registers data structure
				GevGetCameraRegisters( handle, &reg, sizeof(reg));

				//=================================================================
				// Set up a grab/transfer from this camera
				//

#if 1
                UINT32 read;
                printf("writing");
  				GEV_REGISTER camreg ;
  				
  				
				GevRegisterWriteInt(handle, &reg.PixelFormat, 0, 0x0108000A); //BayerGB8
				//GevRegisterWriteInt(handle, &reg.TriggerMode, 0, 0);
				//GevRegisterWriteInt(handle, &reg.BinningHorizontal, 0, 1);
				//GevRegisterWriteInt(handle, &reg.BinningVertical, 0, 1);
				GevRegisterWriteInt(handle, &reg.OffsetX, 0, 0);
				GevRegisterWriteInt(handle, &reg.OffsetY, 0, 0);
				GevRegisterWriteInt(handle, &reg.Width, 0, 4096);
				GevRegisterWriteInt(handle, &reg.Height, 0, 3072);
  				
				GevGetRegisterByName(handle, "AcquisitionFrameRateRaw", &camreg);
				GevRegisterWriteInt(handle, &camreg, 0, cFRAMERATE) ; //8 Hz
				GevRegisterReadInt(handle,&camreg, 0, &read);
				
                GevGetRegisterByName(handle, "autoBrightnessMode", &camreg);
				GevRegisterWriteInt(handle, &camreg, 0, 0) ; //active
				GevRegisterReadInt(handle,&camreg, 0, &read);
				GevGetRegisterByName(handle, "autoBrightnessSequence", &camreg);
				GevRegisterWriteInt(handle, &camreg, 0, 0) ; //Exposure_Gain_Iris
				GevRegisterReadInt(handle,&camreg, 0, &read);
				GevGetRegisterByName(handle, "autoBrightnessTargetSource", &camreg);
				GevRegisterWriteInt(handle, &camreg, 0, 1) ; //RawBayerPattern
				GevGetRegisterByName(handle, "autoBrightnessTarget", &camreg);
				GevRegisterWriteInt(handle, &camreg, 0, 50) ; 
				GevRegisterReadInt(handle,&camreg, 0, &read);
				GevGetRegisterByName(handle, "autoBrightnessTargetRangeVariation", &camreg);
				GevRegisterWriteInt(handle, &camreg, 0, 10) ; 
				GevRegisterReadInt(handle,&camreg, 0, &read);
				GevRegisterReadInt(handle,&camreg, 0, &read);
				GevGetRegisterByName(handle, "autoBrightnessAlgorithm", &camreg);
				GevRegisterWriteInt(handle, &camreg, 0, 1) ; //HistogramWindowing
				GevRegisterReadInt(handle,&camreg, 0, &read);
				GevGetRegisterByName(handle, "autoBrightnessAlgoHistogramWindowingLowerBoundary", &camreg);
				GevRegisterWriteInt(handle, &camreg, 0, 1) ; //10%
				GevRegisterReadInt(handle,&camreg, 0, &read);
				GevGetRegisterByName(handle, "autoBrightnessAlgoHistogramWindowingUpperBoundary", &camreg);
				GevRegisterWriteInt(handle, &camreg, 0, 99) ; //90%
				GevRegisterReadInt(handle,&camreg, 0, &read);
				GevGetRegisterByName(handle, "autoBrightnessAlgoSource", &camreg);
				GevRegisterWriteInt(handle, &camreg, 0, 0) ; //local
				GevRegisterReadInt(handle,&camreg, 0, &read);
				GevGetRegisterByName(handle, "autoBrightnessAlgoMinTimeActivationRaw", &camreg);
				GevRegisterWriteInt(handle, &camreg, 0, 1) ; //0 sec 
				GevRegisterReadInt(handle,&camreg, 0, &read);
				GevGetRegisterByName(handle, "autoBrightnessAlgoConvergenceTimeRaw", &camreg);
				GevRegisterWriteInt(handle, &camreg, 0, 2000000) ;  //2 sec
				GevRegisterReadInt(handle,&camreg, 0, &read);
				
    			GevGetRegisterByName(handle, "ExposureAuto", &camreg);
				GevRegisterWriteInt(handle, &camreg, 0, 0 ) ; //fixed
				GevGetRegisterByName(handle, "ExposureMode", &camreg);
				GevRegisterWriteInt(handle, &camreg, 0, 0 ) ; //Timed					
				GevGetRegisterByName(handle, "exposureAlignment", &camreg);
				GevRegisterWriteInt(handle, &camreg, 0, 0) ; //Synchronous
				GevGetRegisterByName(handle, "exposureDelay", &camreg);
				GevRegisterWriteInt(handle, &camreg, 0, 0) ; //0us
				GevRegisterReadInt(handle,&camreg, 0, &read);
				GevGetRegisterByName(handle, "ExposureTimeRaw", &camreg);
				GevRegisterWriteInt(handle, &camreg, 0, 60000) ; //10000us
//    			GevGetRegisterByName(handle, "ExposureAuto", &camreg);
//				GevRegisterWriteInt(handle, &camreg, 0, 2 ) ; //Continuous
//				GevRegisterReadInt(handle,&camreg, 0, &read);
    			//write boundary for min and max exposures
#define EXPOSURE_AUTO_MAX_VALUE_REG_ADDRESS (0x200056dc)
//    			Gev_WriteReg (handle, EXPOSURE_AUTO_MAX_VALUE_REG_ADDRESS , __bswap_32( 100000) ); //100000 us
#define EXPOSURE_AUTO_MIN_VALUE_REG_ADDRESS (0x2000598c)    			
//    			Gev_WriteReg (handle, EXPOSURE_AUTO_MIN_VALUE_REG_ADDRESS , __bswap_32( 500 )); //500 us
    			//This don't work
    			//GevGetRegisterByName(handle, "exposureAutoMaxValue", &camreg);
				//GevRegisterWriteInt(handle, &camreg, 0, 100000) ; //100000 us
				//GevGetRegisterByName(handle, "exposureAutoMinValue", &camreg);
				//GevRegisterWriteInt(handle, &camreg, 0, 1000 ) ; //500 us
				//GevRegisterReadInt(handle,&camreg, 0, &read);

				GevGetRegisterByName(handle, "GainSelector", &camreg);
				GevRegisterWriteInt(handle, &camreg, 0, 0) ; //AnalogAll
				GevRegisterReadInt(handle,&camreg, 0, &read);
                GevGetRegisterByName(handle, "GainRaw", &camreg);
				GevRegisterWriteInt(handle, &camreg, 0, 0); //1
				GevGetRegisterByName(handle, "GainAuto", &camreg);
				GevRegisterWriteInt(handle, &camreg, 0, 0) ; //Continuous
				GevGetRegisterByName(handle, "gainAutoMaxValueRaw", &camreg);
				GevRegisterWriteInt(handle, &camreg, 0, (4-1)*512) ; //4
				GevGetRegisterByName(handle, "gainAutoMinValueRaw", &camreg);
				GevRegisterWriteInt(handle, &camreg, 0, 0) ; //1

				
			
				/*
				GevGetRegisterByName(handle, "autoBrightnessROIMode", &camreg);
				GevRegisterWriteInt(handle, &camreg, 0, 0) ; //off
				GevGetRegisterByName(handle, "autoBrightnessAlgoSource", &camreg);
				GevRegisterWriteInt(handle, &camreg, 0, 0) ; //local on camera
				*/  
				/*
				//repeating because ???
                GevGetRegisterByName(handle, "autoBrightnessMode", &camreg);
				GevRegisterWriteInt(handle, &camreg, 0, 1) ; //active
				GevRegisterReadInt(handle,&camreg, 0, &read);
				GevGetRegisterByName(handle, "autoBrightnessSequence", &camreg);
				GevRegisterWriteInt(handle, &camreg, 0, 0) ; //Exposure_Gain_Iris
				GevRegisterReadInt(handle,&camreg, 0, &read);
				GevGetRegisterByName(handle, "autoBrightnessTargetSource", &camreg);
				GevRegisterWriteInt(handle, &camreg, 0, 1) ; //RawBayerPattern
				GevGetRegisterByName(handle, "autoBrightnessTarget", &camreg);
				GevRegisterWriteInt(handle, &camreg, 0, 50) ; 
				GevRegisterReadInt(handle,&camreg, 0, &read);
				GevGetRegisterByName(handle, "autoBrightnessTargetRangeVariation", &camreg);
				GevRegisterWriteInt(handle, &camreg, 0, 10) ; 
				GevRegisterReadInt(handle,&camreg, 0, &read);
				GevRegisterReadInt(handle,&camreg, 0, &read);
				GevGetRegisterByName(handle, "autoBrightnessAlgorithm", &camreg);
				GevRegisterWriteInt(handle, &camreg, 0, 1) ; //HistogramWindowing
				GevRegisterReadInt(handle,&camreg, 0, &read);
				GevGetRegisterByName(handle, "autoBrightnessAlgoHistogramWindowingLowerBoundary", &camreg);
				GevRegisterWriteInt(handle, &camreg, 0, 1) ; //10%
				GevRegisterReadInt(handle,&camreg, 0, &read);
				GevGetRegisterByName(handle, "autoBrightnessAlgoHistogramWindowingUpperBoundary", &camreg);
				GevRegisterWriteInt(handle, &camreg, 0, 99) ; //90%
				GevRegisterReadInt(handle,&camreg, 0, &read);
				GevGetRegisterByName(handle, "autoBrightnessAlgoSource", &camreg);
				GevRegisterWriteInt(handle, &camreg, 0, 0) ; //local
				GevRegisterReadInt(handle,&camreg, 0, &read);
				GevGetRegisterByName(handle, "autoBrightnessAlgoMinTimeActivationRaw", &camreg);
				GevRegisterWriteInt(handle, &camreg, 0, 1) ; //0 sec 
				GevRegisterReadInt(handle,&camreg, 0, &read);
				GevGetRegisterByName(handle, "autoBrightnessAlgoConvergenceTimeRaw", &camreg);
				GevRegisterWriteInt(handle, &camreg, 0, 2000000) ;  //2 sec
				GevRegisterReadInt(handle,&camreg, 0, &read);
				*/
				
			


//                GevGetRegisterByName(handle, "autoBrightnessMode", &camreg);
//				GevRegisterWriteInt(handle, &camreg, 0, 1) ; //active

                printf("done");
                
#if 1
                //debug by reading all the settings set if they are good
                GevGetRegisterByName(handle, "AcquisitionFrameRateRaw", &camreg);
				//GevRegisterWriteInt(handle, &camreg, 0, cFRAMERATE) ; //8 Hz
				GevRegisterReadInt(handle,&camreg, 0, &read);
				GevGetRegisterByName(handle, "exposureDelay", &camreg);
				//GevRegisterWriteInt(handle, &camreg, 0, 0) ; //0us
				GevRegisterReadInt(handle,&camreg, 0, &read);
				GevGetRegisterByName(handle, "ExposureTimeRaw", &camreg);
				//GevRegisterWriteInt(handle, &camreg, 0, 1000) ; //10000us
				GevRegisterReadInt(handle,&camreg, 0, &read);
    			
    			Gev_ReadReg (handle, EXPOSURE_AUTO_MIN_VALUE_REG_ADDRESS, &read);
    			Gev_ReadReg (handle, EXPOSURE_AUTO_MAX_VALUE_REG_ADDRESS, &read);
    			//This doesnt work
    			//GevGetRegisterByName(handle, "exposureAutoMaxValue", &camreg);
				//GevRegisterWriteInt(handle, &camreg, 0, 100000) ; //100000 us
				//GevRegisterReadInt(handle,&camreg, 0, &read);
				//GevGetRegisterByName(handle, "exposureAutoMinValue", &camreg);
				//GevRegisterWriteInt(handle, &camreg, 0, 500) ; //500 us
				//GevRegisterReadInt(handle,&camreg, 0, &read);
				
				GevGetRegisterByName(handle, "ExposureMode", &camreg);
				//GevRegisterWriteInt(handle, &camreg, 0, 0) ; //Timed
				GevRegisterReadInt(handle,&camreg, 0, &read);
				GevGetRegisterByName(handle, "exposureAlignment", &camreg);
				//GevRegisterWriteInt(handle, &camreg, 0, 0) ; //Synchronous
				GevRegisterReadInt(handle,&camreg, 0, &read);
				GevGetRegisterByName(handle, "ExposureAuto", &camreg);
				//GevRegisterWriteInt(handle, &camreg, 0, 0 ) ; //Continuous
				GevRegisterReadInt(handle,&camreg, 0, &read);
				GevGetRegisterByName(handle, "GainSelector", &camreg);
				//GevRegisterWriteInt(handle, &camreg, 0, 1) ; //AnalogAll
				GevRegisterReadInt(handle,&camreg, 0, &read);
				GevGetRegisterByName(handle, "GainRaw", &camreg);
				//GevRegisterWriteInt(handle, &camreg, 0, 0); //1
				GevRegisterReadInt(handle,&camreg, 0, &read);
				GevGetRegisterByName(handle, "GainAuto", &camreg);
				//GevRegisterWriteInt(handle, &camreg, 0, 2) ; //Continuous
				GevRegisterReadInt(handle,&camreg, 0, &read);
				GevGetRegisterByName(handle, "gainAutoMaxValueRaw", &camreg);
				//GevRegisterWriteInt(handle, &camreg, 0, (4-1)*512) ; //4
				GevRegisterReadInt(handle,&camreg, 0, &read);
				GevGetRegisterByName(handle, "gainAutoMinValueRaw", &camreg);
				//GevRegisterWriteInt(handle, &camreg, 0, 0) ; //1
				GevRegisterReadInt(handle,&camreg, 0, &read);
				
				//GevRegisterWriteInt(handle, &reg.PixelFormat, 0, 0x0108000A); //BayerGB8
				//GevRegisterWriteInt(handle, &reg.TriggerMode, 0, 0);
				//GevRegisterWriteInt(handle, &reg.BinningHorizontal, 0, 1);
				//GevRegisterWriteInt(handle, &reg.BinningVertical, 0, 1);
				//GevRegisterWriteInt(handle, &reg.OffsetX, 0, 0);
				//GevRegisterWriteInt(handle, &reg.OffsetY, 0, 0);
				//GevRegisterWriteInt(handle, &reg.Width, 0, 4096);
				//GevRegisterWriteInt(handle, &reg.Height, 0, 3072);
				
				
                GevGetRegisterByName(handle, "autoBrightnessMode", &camreg);
				//GevRegisterWriteInt(handle, &camreg, 0, 2) ; //active
				GevRegisterReadInt(handle,&camreg, 0, &read);
				GevGetRegisterByName(handle, "autoBrightnessTarget", &camreg);
				//GevRegisterWriteInt(handle, &camreg, 0, 50) ; 
				GevRegisterReadInt(handle,&camreg, 0, &read);
				GevGetRegisterByName(handle, "autoBrightnessTargetRangeVariation", &camreg);
				//GevRegisterWriteInt(handle, &camreg, 0, 20) ; 
				GevRegisterReadInt(handle,&camreg, 0, &read);
				GevGetRegisterByName(handle, "autoBrightnessTargetSource", &camreg);
				//GevRegisterWriteInt(handle, &camreg, 0, 1) ; //RawBayerPattern
				GevRegisterReadInt(handle,&camreg, 0, &read);
				GevGetRegisterByName(handle, "autoBrightnessAlgorithm", &camreg);
				//GevRegisterWriteInt(handle, &camreg, 0, 1) ; //HistogramWindowing
				GevRegisterReadInt(handle,&camreg, 0, &read);
				GevGetRegisterByName(handle, "autoBrightnessAlgoHistogramWindowingLowerBoundary", &camreg);
				//GevRegisterWriteInt(handle, &camreg, 0, 1) ; //10%
				GevRegisterReadInt(handle,&camreg, 0, &read);
				GevGetRegisterByName(handle, "autoBrightnessAlgoHistogramWindowingUpperBoundary", &camreg);
				//GevRegisterWriteInt(handle, &camreg, 0, 99) ; //90%
				GevRegisterReadInt(handle,&camreg, 0, &read);
				GevGetRegisterByName(handle, "autoBrightnessAlgoMinTimeActivationRaw", &camreg);
				//GevRegisterWriteInt(handle, &camreg, 0, 1) ; //0 sec 
				GevRegisterReadInt(handle,&camreg, 0, &read);
				GevGetRegisterByName(handle, "autoBrightnessAlgoConvergenceTimeRaw", &camreg);
				//GevRegisterWriteInt(handle, &camreg, 0, 2000000) ;  //2 sec
				GevRegisterReadInt(handle,&camreg, 0, &read);
				GevGetRegisterByName(handle, "autoBrightnessSequence", &camreg);
				//GevRegisterWriteInt(handle, &camreg, 0, 0) ; //Exposure_Gain_Iris
				GevRegisterReadInt(handle,&camreg, 0, &read);
				/*
				GevGetRegisterByName(handle, "autoBrightnessROIMode", &camreg);
				GevRegisterWriteInt(handle, &camreg, 0, 0) ; //off
				GevGetRegisterByName(handle, "autoBrightnessAlgoSource", &camreg);
				GevRegisterWriteInt(handle, &camreg, 0, 0) ; //local on camera
				*/
#endif                
	
				GevRegisterReadInt(handle, &reg.AcquisitionFrameRateRaw, 0, &frameRate);
				printf("frameRate (mHz) = %d\n", frameRate);
#endif


				// Get the current image settings in the camera
                status = GevGetImageParameters(handle, &width, &height, &x_offset, &y_offset, &format);
                if (status == 0)
                {

	                printf("Camera ROI set for \n\theight = %d\n\twidth = %d\n\txo = %d\n\tyo = %d, fmt = 0x%08x\n", height,width,x_offset, y_offset, format);

	                maxHeight = height;
	                maxWidth = width;
                    maxDepth = GetPixelSizeInBytes(format);

	                // Allocate image buffers
	                size = maxDepth * maxWidth * maxHeight;
	                for (i = 0; i < numBuffers; i++)
	                {
		                bufAddress[i] = (PUINT8)malloc(size);
		                memset(bufAddress[i], 0, size);
	                }

	                // Boost application RT response (not too high since GEV library boosts data receive thread to max allowed)
	                // SCHED_FIFO can cause many unintentional side effects.
	                // SCHED_RR has fewer side effects.
	                // SCHED_OTHER (normal scheduler) is not too bad afer all.
#if 0
                    {
	                    struct sched_param param = {0};
	                    param.sched_priority = (sched_get_priority_max(SCHED_FIFO) - sched_get_priority_min(SCHED_FIFO)) / 2;
	                    sched_setscheduler(0, SCHED_FIFO, &param); // Don't care if it fails since we can't do anyting about it.
                    }
#endif

                    //Don't start transfer until we receive input to start
//                    printf("\nPress key to start capture \n");
//                    GetKey();
                    
                    //reset counter of timestamp to start now
    				GevGetRegisterByName(handle, "timestampControlReset", &camreg);
	    			GevRegisterWriteInt(handle, &camreg, 0, 1 ) ; //Reset to 0
        			// Timestamp increments each microsecond
        			
        			//Register date on computer for synchronization
        			
        			time_t initial_time = time(NULL);
        			
        			
        			//Initialize jpeg_debayer_sender thread

        			bayerJpegSave_initialize();
        			
                    

                    status = GevInitializeImageTransfer( handle, numBuffers, bufAddress);
                    
                    
                    MY_CONTEXT context = {0};
                    pthread_t  tid;
	                char c;
	                // Create a thread to receive images from the API and save them.
	                context.camHandle = handle;
	                context.depth = GetPixelSizeInBytes(format);
	                context.height = height;
	                context.width = width;
	                context.format = format;
	                context.initial_time = initial_time;
	                context.exit = FALSE;
	                pthread_create(&tid, NULL, ImageSavingThread, &context); 
	                sleep(3);
	                //Start continous Grab
	                for (i = 0; i < numBuffers; i++)
	                {
		                memset(bufAddress[i], 0, size);
	                }
	                status = GevStartImageTransfer( handle, -1); //Non-stop transfer
	                if (status != 0) printf("Error starting grab - 0x%x  or %d\n", status, status); 
	
	                // Call the main command loop
	                // which just wait for cancellation
                    PrintMenu();
                    while(!done)
                    {
                        c = GetKey();
                       
                        //Abort
                        if ((c == 0x1b) || (c == 'q') || (c == 'Q'))
                        {
		                    GevStopImageTransfer(handle);
                            done = TRUE;
		                    context.exit = TRUE;
		                    pthread_join( tid, NULL);      
                        }
                    }
                    
	                GevAbortImageTransfer(handle);
	                status = GevFreeImageTransfer(handle);
	                status = GevSetImageParameters(handle, maxWidth,  maxHeight, x_offset,  y_offset, format);
                    sleep(10); //Way too bad
                    
	                // Don't free the buffers until after the saving thread is down.
	                // (Threads may have queued frames in progress).
	                for (i = 0; i < numBuffers; i++)
	                {	
		                free(bufAddress[i]);
	                }
                }
                else
                {
	                printf("Error : 0x%0x : accessing camera\n", status);
                }
            }
            // Close the camera.
            GevCloseCamera(&handle);
            //close
			bayerJpegSave_cleanup();
        }

    }
        

    // Close down the API.
    GevApiUninitialize();

    // Close socket API
    _CloseSocketAPI ();	// must close API even on error


    //printf("Hit any key to exit\n");
    //kbhit();

	return 0;
}

