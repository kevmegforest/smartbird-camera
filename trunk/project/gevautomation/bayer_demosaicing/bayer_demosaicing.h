#ifndef __bayer_demosaicing_H_
#define __bayer_demosaicing_H_

#define GL_SAVE_NON_COMPRESSED_DEMOSAICED_PICTURE

#define cGLDEBUG

#ifdef cGLDEBUG
#define mGL_GET_ERROR()				\
		glDebugError = glGetError()
#else
#define mGL_GET_ERROR()				\
	do{}while(0)
#endif

//input and output is the argument, gives a RGBA PICTURE
#ifdef GL_SAVE_NON_COMPRESSED_DEMOSAICED_PICTURE
//The RGBAPicture should already be malloced to the good size before being passed
int demosaiceBayer(void * bayerPicture, void * RGBAPicture );
#else
int demosaiceBayer(void * bayerPicture);
#endif

/*
 * Initialize ressource
 */
int initializeGL(void);

int cleanupGL(void);

#endif /* __bayer_demosaicing_H_ */
