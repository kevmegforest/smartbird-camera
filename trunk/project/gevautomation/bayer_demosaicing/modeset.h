#ifndef __MODESET_H__
#define __MODESET_H__
 
#define EGL_EGLEXT_PROTOTYPES //Use for activating screen less ogl context
#include <EGL/egl.h>
#include <EGL/eglext.h> 
 
int intializeOpenGLContext(void);

/*
 * Function to create the EGLImageKHR used for fast read from gpu textures
 */
int createRenderingTextureBuffer(EGLImageKHR * outImage, int width, int height);

int destroyOpenGLContext(void);

#endif /* __MODESET_H__ */
