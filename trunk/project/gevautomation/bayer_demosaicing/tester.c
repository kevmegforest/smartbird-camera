#include <stdio.h>
#include <stdlib.h>
#include <turbojpeg.h>
#include "util.h"
#include "bayer_demosaicing.h"
#include "../CameraPicturesSpec.h"
#include "../ImageSavingUtilities.h"

#include <GL/freeglut.h>

#include "gl_compatibility_3_0.h"
#include "../network_sender/JPEGStreamerClient.h"

#include <unistd.h>

int main(int argc, char** argv)
{
	//Load a dng picture to get it's raw Bayer data
	char filename[256] = "testPicture/testInputData.dng";
	GLint length;
	void * fileData = file_contents( filename, &length);
	if (fileData == 0 || length == 0 )
		return -1;
	glutInit(&argc, argv);
	glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize (250, 250);
	glutInitWindowPosition (0, 0);
	glutCreateWindow ("awesome window");
	
	//initialise OpenGL
	initializeGL();
	//initializeNetwork
	netStreamer_initialize();
	//render
#ifdef GL_SAVE_NON_COMPRESSED_DEMOSAICED_PICTURE
	//data from DNG is at position 186 --See gevconsoleDemo.c
	void * RGBAPicture = malloc( sizeof(char) * cPICTURE_HEIGHT * cPICTURE_WIDTH * 4);
	//The RGBAPicture should already be malloced to the good size before being passed
	int result = demosaiceBayer(&fileData[186] , RGBAPicture );
	//Save picture
	//SaveRGBAImageFileToDNG( 0, cPICTURE_WIDTH, cPICTURE_HEIGHT, 8, 0, 0, 0, RGBAPicture)

	//send picture

#else
	int result = demosaiceBayer(fileData[186);
#endif
	
	///////////////JPEG////////////
	const int JPEG_QUALITY = 75;
	//const int COLOR_COMPONENTS = 3;
	//int _width = 1920;
	//int _height = 1080;
	long unsigned int _jpegSize = 0;
	unsigned char* _compressedImage = NULL; //!< Memory is allocated by tjCompress2 if _jpegSize == 0
	
	tjhandle _jpegCompressor = tjInitCompress();
	
	tjCompress2(_jpegCompressor, RGBAPicture, cPICTURE_WIDTH, 0, cPICTURE_HEIGHT, TJPF_RGBA,
	          &_compressedImage, &_jpegSize, TJSAMP_444, JPEG_QUALITY,
	          TJFLAG_ACCURATEDCT);
	
	tjDestroy(_jpegCompressor);
	
	SaveJPEGImageFile(cPICTURE_FORMAT,cPICTURE_WIDTH, cPICTURE_HEIGHT, 8,0,0,0,0, _compressedImage , _jpegSize * sizeof(char));


	netStreamer_addPictureToSend(_compressedImage, _jpegSize * sizeof(char),19,46,10,50);

	//TODO: Save dans un fichier .jpg
	
	//to free the memory allocated by TurboJPEG (either by tjAlloc(), 
	//or by the Compress/Decompress) after you are done working on it:
	tjFree(_compressedImage);
//	printf("\n1");
	free(fileData);
//	printf("\n2");
	//usleep(1000000);
	netStreamer_cleanup();
	free(RGBAPicture);
	RGBAPicture = NULL;
	return result;	

}
