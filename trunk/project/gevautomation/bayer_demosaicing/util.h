#ifndef __DEBAYER_UTIL_H__
#define __DEBAYER_UTIL_H__

#include "gl_compatibility_3_0.h"

void * file_contents(const char *filename, GLint *length);

#endif // __DEBAYER_UTIL_H__
