/*
 * modeset - DRM Modesetting Example
 *
 * Written 2012 by David Herrmann <dh.herrmann@googlemail.com>
 * Dedicated to the Public Domain.
 */

/*
 * DRM Modesetting Howto
 * This document describes the DRM modesetting API. Before we can use the DRM
 * API, we have to include xf86drm.h and xf86drmMode.h. Both are provided by
 * libdrm which every major distribution ships by default. It has no other
 * dependencies and is pretty small.
 *
 * Please ignore all forward-declarations of functions which are used later. I
 * reordered the functions so you can read this document from top to bottom. If
 * you reimplement it, you would probably reorder the functions to avoid all the
 * nasty forward declarations.
 *
 * For easier reading, we ignore all memory-allocation errors of malloc() and
 * friends here. However, we try to correctly handle all other kinds of errors
 * that may occur.
 *
 * All functions and global variables are prefixed with "modeset_*" in this
 * file. So it should be clear whether a function is a local helper or if it is
 * provided by some external library.
 */

#include "modeset.h"

#define _GNU_SOURCE
#include <errno.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <xf86drm.h>
#include <xf86drmMode.h>

#include <gbm.h>

#include <string.h>
//#include <sys/mman.h>
//#include <time.h>
#include <unistd.h>


/*
 * Declare as global the information related to the opengl 
 * Context and device initialisation
 *
 */

 static int g_fd = 0;
 static struct gbm_device * g_gbm = NULL;
 static EGLDisplay g_dpy = NULL;
 static EGLContext g_ctx = NULL;
 
 //array to store every gmb_bo reference to delete it later
 static struct gbm_bo * g_boArray[16];
 static int g_boArrayPosition = 0;
 //TODO do a real stack this is dangerous
 
 
/*
 * When the linux kernel detects a graphics-card on your machine, it loads the
 * correct device driver (located in kernel-tree at ./drivers/gpu/drm/<xy>) and
 * provides two character-devices to control it. Udev (or whatever hotplugging
 * application you use) will create them as:
 *     /dev/dri/card0
 *     /dev/dri/controlID64
 * We only need the first one. You can hard-code this path into your application
 * like we do here, but it is recommended to use libudev with real hotplugging
 * and multi-seat support. However, this is beyond the scope of this document.
 * Also note that if you have multiple graphics-cards, there may also be
 * /dev/dri/card1, /dev/dri/card2, ...
 *
 * We simply use /dev/dri/card0 here but the user can specify another path on
 * the command line.
 *
 * modeset_open(out, node): This small helper function opens the DRM device
 * which is given as @node. The new fd is stored in @out on success. On failure,
 * a negative error code is returned.
 * After opening the file, we also check for the DRM_CAP_DUMB_BUFFER capability.
 * If the driver supports this capability, we can create simple memory-mapped
 * buffers without any driver-dependent code. As we want to avoid any radeon,
 * nvidia, intel, etc. specific code, we depend on DUMB_BUFFERs here.
 */

static int modeset_open(int *out, const char *node)
{
	int fd, ret;
	uint64_t has_dumb;

	fd = open(node, O_RDWR | O_CLOEXEC);
	if (fd < 0) {
		ret = -errno;
		fprintf(stderr, "cannot open '%s': %m\n", node);
		return ret;
	}

	if (drmGetCap(fd, DRM_CAP_DUMB_BUFFER, &has_dumb) < 0 ||
	    !has_dumb) {
		fprintf(stderr, "drm device '%s' does not support dumb buffers\n",
			node);
		close(fd);
		return -EOPNOTSUPP;
	}

	*out = fd;
	return 0;
}

/*
 * As a next step we need to initialize the GBM library
 * and the EGL display
 *
 * Gives 2 output which will be useful for further init
 * GBM is for Generic Buffer Management  to create HW acclerated Framebuffer for Mesa
 * EGLDisplay is the EGL display handle
 *///TODO Manage errors
static int modeset_GBM_EGL_init( int fd, struct gbm_device * gbmOut, EGLDisplay * dpyOut )
{
	gbmOut = gbm_create_device( fd);
	*(dpyOut) = eglGetDisplay((EGLNativeDisplayType) gbmOut);
	
	EGLint major, minor;
	const char * ver , * extensions;
	eglInitialize( *dpyOut, &major, &minor );
	ver = eglQueryString(*dpyOut, EGL_VERSION);
	fprintf(stderr, "EGL_VERSION = %s", ver);
	extensions = eglQueryString( *dpyOut, EGL_EXTENSIONS );
	
	//Verify extensions for hw acceleration without window manager
	if (!strstr(extensions, "EGL_KHR_surfaceless_context")) 
	{
    	fprintf(stderr, "no surfaceless support, cannot initialize\n");
    	return(-1);
	}
	return 0;
}

/*
 * Finally! We have a connector with a suitable CRTC. We know which mode we want
 * to use and we have a framebuffer of the correct size that we can write to.
 * There is nothing special left to do. We only have to program the CRTC to
 * connect each new framebuffer to each selected connector for each combination
 * that we saved in the global modeset_list.
 * This is done with a call to drmModeSetCrtc().
 *
 * So we are ready for our main() function. First we check whether the user
 * specified a DRM device on the command line, otherwise we use the default
 * /dev/dri/card0. Then we open the device via modeset_open(). modeset_prepare()
 * prepares all connectors and we can loop over "modeset_list" and call
 * drmModeSetCrtc() on every CRTC/connector combination.
 *
 * But printing empty black pages is boring so we have another helper function
 * modeset_draw() that draws some colors into the framebuffer for 5 seconds and
 * then returns. And then we have all the cleanup functions which correctly free
 * all devices again after we used them. All these functions are described below
 * the main() function.
 *
 * As a side note: drmModeSetCrtc() actually takes a list of connectors that we
 * want to control with this CRTC. We pass only one connector, though. As
 * explained earlier, if we used multiple connectors, then all connectors would
 * have the same controlling framebuffer so the output would be cloned. This is
 * most often not what you want so we avoid explaining this feature here.
 * Furthermore, all connectors will have to run with the same mode, which is
 * also often not guaranteed. So instead, we only use one connector per CRTC.
 *
 * Before calling drmModeSetCrtc() we also save the current CRTC configuration.
 * This is used in modeset_cleanup() to restore the CRTC to the same mode as was
 * before we changed it.
 * If we don't do this, the screen will stay blank after we exit until another
 * application performs modesetting itself.
 */
 
int intializeOpenGLContext(void)
{
	int result;
	const char *gpuCard = "/dev/dri/card0";

	/* open the DRM device */
	result = modeset_open(&g_fd, gpuCard);
	if (result)
		return result;
		

	//Initialize OpenGL context
	result = modeset_GBM_EGL_init( g_fd , g_gbm, &g_dpy );
	if (result)
	{
		close(g_fd);
		return result;
	}
	eglBindAPI(EGL_OPENGL_API);
	g_ctx = eglCreateContext(g_dpy, NULL, EGL_NO_SURFACE, NULL);
	
	eglMakeCurrent(g_dpy, EGL_NO_SURFACE, EGL_NO_SURFACE, g_ctx);
	
	result = 0;
	return result;
}

/*
 * Function to create the EGLImageKHR used for fast read from gpu textures
 */
int createRenderingTextureBuffer(EGLImageKHR * outImage, int width, int height)
{
	//Create a buffer 
	g_boArray[g_boArrayPosition] = gbm_bo_create(g_gbm, height, width, GBM_BO_FORMAT_XRGB8888, GBM_BO_USE_RENDERING);
	
	//Create Image
	outImage = eglCreateImageKHR(g_dpy, NULL, EGL_NATIVE_PIXMAP_KHR, (( EGLClientBuffer) g_boArray[g_boArrayPosition]), NULL); 
	
	g_boArrayPosition++;
	//TODO The EGLIMAGEKHR also need to be deleted
	//TODO If we want to read the image content we need to give the gbm_bo associated with the image or at least the poointer to it's data
	return 0;
}


int destroyOpenGLContext(void)
{
	//TODO Destroy the EGLIMAGEKHR before destroying the gbm_bo
	while( g_boArrayPosition > 0 )
	{
		g_boArrayPosition--;
		gbm_bo_destroy( g_boArray[g_boArrayPosition]);
	}
	
	//Close device
	
	close( g_fd);
	
	return 0;
}
