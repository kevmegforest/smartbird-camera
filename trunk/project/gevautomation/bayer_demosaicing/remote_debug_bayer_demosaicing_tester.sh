#!/bin/bash
remote=192.168.1.25
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR

# Copy the root lib folder of target to host
mkdir -p remote_root/lib64/
mkdir -p remote_root/usr/lib64/dri

#rsync -rL  kevmegforest@$remote:/lib64/ remote_root/lib64/
#rsync -rL  kevmegforest@$remote:/usr/lib64/dri/ remote_root/usr/lib64/dri/

#make program and launch gdb server

ssh kevmegforest@$remote 'cd  workspace/smartbird-camera/trunk/project/gevautomation/bayer_demosaicing/; git pull ; make '
#clean ; make'
echo "ssh -t kevmegforest@$remote 'cd  workspace/smartbird-camera/trunk/project/gevautomation/bayer_demosaicing/ ; sudo sh -c ' export LD_LIBRARY_PATH=../network_sender ; gdbserver localhost:12345 bayer_demosaicing_tester; bash'; bash '" > gdbserver_remote_run.sh
gnome-terminal -x sh -c "./gdbserver_remote_run.sh" &

#copy program to host
rsync kevmegforest@$remote:workspace/smartbird-camera/trunk/project/gevautomation/bayer_demosaicing/bayer_demosaicing_tester bayer_demosaicing_tester_remote
rsync kevmegforest@$remote:workspace/smartbird-camera/trunk/project/gevautomation/network_sender/libudt.so ../libudt.so

#Launch gdb with remote settings

echo "target remote $remote:12345" > gdbcommands
echo "set solib-absolute-prefix $DIR/remote_root" >> gdbcommands
echo "b main" >> gdbcommands
echo "continue" >> gdbcommands
#gdb -x gdbcommands bayer_demosaicing_tester_remote
