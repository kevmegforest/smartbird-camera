/*
 * bayer_jpeg_and_save.h
 *
 *  Created on: Jun 16, 2014
 *      Author: kevmegforest
 */

#ifndef BAYER_JPEG_AND_SAVE_H_
#define BAYER_JPEG_AND_SAVE_H_

#include <stdint.h>

void bayerJpegSave_initialize();
void bayerJpegSave_cleanup();

void bayerJpegSave_addRawBayerPicture(void * f_pictureData, uint8_t f_hour, uint8_t f_minute, uint8_t f_second, uint16_t f_millisecond);




#endif /* BAYER_JPEG_AND_SAVE_H_ */
