#include "bayer_demosaicing.h"

#include <stdlib.h>
#include <string.h>

#include <math.h>
#include <stdio.h>

#include "util.h"

#include "../CameraPicturesSpec.h"

#include "gl_compatibility_3_0.h"

#include <assert.h>

#include <GL/freeglut.h>

/** FOR debugging purposes */
#ifdef cGLDEBUG
	GLenum glDebugError = 0 ;
#endif

static struct {
    /*GLuint pixelCoordBuffer; //used to store the coordinates of the pixels to be sampled
    GLuint elementBuffer; 
    GLuint pixelBufferObject[2]; //used to store pictures before putting them into texture units
    GLuint textureBayerPict[2]; // Input image with bayer format
    GLuint textureRGBA8Pict[2];  //Output image demosaiced
    GLuint framebufferObject; 
    GLuint vertex_shader, fragment_shader, program;
    
    struct {
        GLint sourceSize;
        GLint firstRed;
        GLint textureBayerPict;
    } uniforms;
    
    struct {
        GLint pixelCoord;
    } attributes;*/
    GLuint fbo;
	GLuint fboTexture1;
	GLuint fboTexture2;
	GLuint bayerTexture;
	GLuint shaderProgram;
	GLuint vertexShader;
	GLuint fragmentShader;
} g_resources;

static void show_info_log(
    GLuint object,
    void (* glGet__iv) (GLuint, GLenum, GLint*),
    void (* glGet__InfoLog) (GLuint, GLsizei, GLsizei*, GLchar*)
)
{
    GLint log_length;
    char *log;

    glGet__iv(object, GL_INFO_LOG_LENGTH, &log_length);
    log = malloc(log_length);
    glGet__InfoLog(object, log_length, NULL, log);
    fprintf(stderr, "%s", log);
    free(log);
}



int initializeShaders(const char *vertexSrcFilename, const char *fragmentSrcFilename)
{
    GLint lengthVertex;	
    GLchar *vertexSrc = file_contents(vertexSrcFilename, &lengthVertex);
    GLint shader_ok;
    if (!vertexSrc) return -1;
    g_resources.vertexShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(g_resources.vertexShader, 1, (const GLchar**)&vertexSrc, &lengthVertex);
    free(vertexSrc);
    glCompileShader(g_resources.vertexShader);
    glGetShaderiv(g_resources.vertexShader, GL_COMPILE_STATUS, &shader_ok);
    if (!shader_ok) {
        fprintf(stderr, "Failed to compile %s:\n", vertexSrcFilename);
        show_info_log(g_resources.vertexShader, glGetShaderiv, glGetShaderInfoLog);
        glDeleteShader(g_resources.vertexShader);
        return 1;
    }

	GLint lengthFragment;	
    GLchar *fragmentSrc = file_contents(fragmentSrcFilename, &lengthFragment);
    if (!fragmentSrc) return -2;
    g_resources.fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(g_resources.fragmentShader, 1, (const GLchar**)&fragmentSrc, &lengthFragment);
    free(fragmentSrc);
    glCompileShader(g_resources.fragmentShader);
    glGetShaderiv(g_resources.fragmentShader, GL_COMPILE_STATUS, &shader_ok);
    if (!shader_ok) {
        fprintf(stderr, "Failed to compile %s:\n", fragmentSrcFilename);
        show_info_log(g_resources.fragmentShader, glGetShaderiv, glGetShaderInfoLog);
        glDeleteShader(g_resources.fragmentShader);
        return 2;
    }
    return 0;
}

void initializeProgram()
{
    GLint program_ok;
    g_resources.shaderProgram = glCreateProgram();
    glAttachShader(g_resources.shaderProgram, g_resources.vertexShader);
    glAttachShader(g_resources.shaderProgram, g_resources.fragmentShader);
    glLinkProgram(g_resources.shaderProgram);
    glGetProgramiv(g_resources.shaderProgram, GL_LINK_STATUS, &program_ok);
    if (!program_ok) {
        fprintf(stderr, "Failed to link shader program:\n");
        show_info_log(g_resources.shaderProgram, glGetProgramiv, glGetProgramInfoLog);
        glDeleteProgram(g_resources.shaderProgram);
        return;
    }
}

int initializeFBO()
{
	glGenFramebuffers(1, &g_resources.fbo);
    glBindFramebuffer(GL_FRAMEBUFFER, g_resources.fbo); //Use for read and draw
    
    glGenTextures(1, &g_resources.fboTexture1);
    glBindTexture(GL_TEXTURE_2D, g_resources.fboTexture1);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); 
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); 
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, cPICTURE_WIDTH, cPICTURE_HEIGHT, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, g_resources.fboTexture1, 0);

	glGenTextures(1, &g_resources.fboTexture2);
    glBindTexture(GL_TEXTURE_2D, g_resources.fboTexture2);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); 
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); 
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST); //LINEAR si possible
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST); //LINEAR si possible
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, cPICTURE_WIDTH, cPICTURE_HEIGHT, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, g_resources.fboTexture2, 0);
    
    glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
    //Verify that's good
   if( glCheckFramebufferStatus(GL_FRAMEBUFFER))
	   return -1;
   return 0;
	//glBindFramebuffer(GL_FRAMEBUFFER, 0);
}


int initializeGL()
{
	//intializeOpenGLContext();	
	
    if( ogl_LoadFunctions() == ogl_LOAD_FAILED )
    {
        fprintf(stderr, "OpenGL 3.0 not available\n");
        return 1;
    }

    mGL_GET_ERROR();

	
	glEnable(GL_TEXTURE_2D);
	initializeShaders("shaders/debayer.v.glsl", "shaders/debayer.f.glsl");
	initializeProgram();
	initializeFBO();
	glGenTextures(1, &g_resources.bayerTexture);

	glClearColor(0.0, 0.0, 0.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);
    glLineWidth(1);
    return 0;
};

int cleanupGL()
{
	return 0 ;//destroyOpenGLContext();
}



int demosaiceBayer(void * bayerPicture, void *rgbaPicture)
{	

//	printf("%u_%u_%u_%u_%u\n",((uint8_t*)bayerPicture)[0],((uint8_t*)bayerPicture)[1],((uint8_t*)bayerPicture)[2],((uint8_t*)bayerPicture)[3],((uint8_t*)bayerPicture)[4] );
	glBindFramebuffer(GL_FRAMEBUFFER, g_resources.fbo);
	glDrawBuffer(GL_COLOR_ATTACHMENT0);
	glReadBuffer(GL_COLOR_ATTACHMENT0);
    glViewport(0, 0, (GLint)cPICTURE_WIDTH, (GLint) cPICTURE_HEIGHT); //-> pas de rendu dans une fenetre
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0.0f, cPICTURE_WIDTH, 0.0f, cPICTURE_HEIGHT, 0.0f, 1.0f);


		glUseProgram(g_resources.shaderProgram);
		//load bayerPicture in texture unit 0
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
		glPixelStorei(GL_PACK_ALIGNMENT, 4);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, g_resources.bayerTexture);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexImage2D(GL_TEXTURE_2D, 0,
					 GL_LUMINANCE8, cPICTURE_WIDTH, cPICTURE_HEIGHT, 0,
					 GL_LUMINANCE, GL_UNSIGNED_BYTE, bayerPicture);
		//glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		printf("%u", glGetError());
		assert(glGetError() == GL_NO_ERROR);

		//execute shaders
		glUniform1i(glGetUniformLocation(g_resources.shaderProgram, "textureBayerPict"), 0);
		glUniform2f(glGetUniformLocation(g_resources.shaderProgram, "firstRed"), 1.0f , 1.0f);
		glUniform4f(glGetUniformLocation(g_resources.shaderProgram, "sourceSize"), 
					(float) cPICTURE_WIDTH,
					(float) cPICTURE_HEIGHT,
					(float) 1/cPICTURE_WIDTH,
					(float) 1/cPICTURE_HEIGHT
					);
		assert(glGetError() == GL_NO_ERROR);

		//draw quad
		//glClear(GL_COLOR_BUFFER_BIT);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		//glColor4f(1.0, 1.0, 1.0, 1.0);
		glBegin(GL_QUADS);
		glTexCoord2f(0.0f, cPICTURE_HEIGHT);
		glVertex2f(0.0f, cPICTURE_HEIGHT);
		glTexCoord2f(cPICTURE_WIDTH, cPICTURE_HEIGHT);
		glVertex2f(cPICTURE_WIDTH, cPICTURE_HEIGHT);
		glTexCoord2f(cPICTURE_WIDTH, 0.0f);
		glVertex2f(cPICTURE_WIDTH, 0.0f);
		glTexCoord2f(0.0f, 0.0f);
		glVertex2f(0.0f, 0.0f);
		glEnd();

		glFlush();
		assert(glGetError() == GL_NO_ERROR);

		//glReadPixels(0,0, cPICTURE_WIDTH, cPICTURE_HEIGHT, GL_RGBA, GL_UNSIGNED_BYTE, rgbaPicture);
		glBindTexture(GL_TEXTURE_2D,g_resources.fboTexture1);
		glGetTexImage(GL_TEXTURE_2D,0,GL_RGBA,GL_UNSIGNED_BYTE,rgbaPicture);
		glBindTexture(GL_TEXTURE_2D,0);
		assert(glGetError() == GL_NO_ERROR);
		//printf("%u\n", glCheckFramebufferStatus(g_resources.fbo));
	glBindFramebuffer(GL_FRAMEBUFFER, 0);



	return 0;
}
/*
 * Global data used by our render callback:glVertexPointer
 */


//TODO Do this with malloc than free it so we can reclaim this memory after Download to GPU
//Data used for attributes
/*static GLfloat g_pixelCoordBufferData[cPICTURE_HEIGHT * cPICTURE_WIDTH * 4] = {0};
static GLuint g_elementBufferData[cPICTURE_HEIGHT * cPICTURE_WIDTH ] = {0};

static int g_renderStage = 0;
static GLuint make_shader(GLenum type, const char *filename)
{
    GLint length;
    GLchar * source = file_contents(filename, &length);
    GLuint shader;
    GLint shader_ok;

    if (!source)
        return 0;

    shader = glCreateShader(type);
    glShaderSource(shader, 1, (const GLchar**)&source, &length);
    free(source);
    glCompileShader(shader);

    glGetShaderiv(shader, GL_COMPILE_STATUS, &shader_ok);
    if (!shader_ok) {
        fprintf(stderr, "Failed to compile %s:\n", filename);
        show_info_log(shader, glGetShaderiv, glGetShaderInfoLog);
        glDeleteShader(shader);
        return 0;
    }
    return shader;
}

static GLuint make_program(GLuint vertex_shader, GLuint fragment_shader)
{
    GLint program_ok;

    GLuint program = glCreateProgram();

    glAttachShader(program, vertex_shader);
    glAttachShader(program, fragment_shader);
    glLinkProgram(program);

    glGetProgramiv(program, GL_LINK_STATUS, &program_ok);
    if (!program_ok) {
        fprintf(stderr, "Failed to link shader program:\n");
        show_info_log(program, glGetProgramiv, glGetProgramInfoLog);
        glDeleteProgram(program);
        return 0;
    }
    return program;
}*/


/*
 * Functions for creating OpenGL objects:
 */
 /*

static GLuint make_framebufferObject()
{
    GLuint fbo;
    
    glGenFramebuffers(1, &fbo);
    glBindFramebuffer(GL_FRAMEBUFFER, fbo); //Use for read and draw
    
    //This needs openGL 4.2 , we don't have it and should then use pbo instead
    //glFramebufferParameteri(GL_FRAMEBUFFER, GL_FRAMEBUFFER_DEFAULT_WIDTH, cPICTURE_WIDTH);
    //glFramebufferParameteri(GL_FRAMEBUFFER, GL_FRAMEBUFFER_DEFAULT_HEIGHT, cPICTURE_HEIGHT);

    //Bind texture to it
    glBindTexture(GL_TEXTURE_2D, firstTextureToBind);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, firstTextureToBind, 0);
    
    glBindTexture(GL_TEXTURE_2D, secondTextureToBind);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, secondTextureToBind, 0);
    
    //Verify that's good
    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    
    //Unbind
    glBindTexture(GL_TEXTURE_2D, 0);
    
    //Keep the framebuffer object bound
    //glBindFramebuffer(GL_FRAMEBUFFER, 0);
    
    if (status != GL_FRAMEBUFFER_COMPLETE)
        return 0; //Error 
    else
        return fbo;
}*/
/*
static GLuint make_staticBuffer(
    GLenum target,
    const void *buffer_data,
    GLsizei buffer_size
) {
    GLuint buffer;
    glGenBuffers(1, &buffer);
    glBindBuffer(target, buffer);
    glBufferData(target, buffer_size, buffer_data, GL_STATIC_DRAW);
    //Unbind
    glBindBuffer(target, 0);
    return buffer;
}*/

/*
 *  Create Pixel Buffer Object for texture
 */
/*
void initializePBO(const void *buffer_data, GLsizei buffer_size, GLenum usage) 
{    
    glGenBuffers(1, &PBO);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, PBO);
    glBufferData(GL_PIXEL_UNPACK_BUFFER, buffer_size, buffer_data, usage);
    //Unbind
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
    return PBO;
}

void update_PBOtexture( GLuint PBO, GLuint texture, GLenum format, void * newData, size_t size)
{
	mGL_GET_ERROR();
    glBindBuffer( GL_PIXEL_UNPACK_BUFFER, PBO);
    mGL_GET_ERROR();
    void * ptr = glMapBuffer(GL_PIXEL_UNPACK_BUFFER, GL_WRITE_ONLY);
    mGL_GET_ERROR();
    memcpy( ptr, newData, size);
    glUnmapBuffer(GL_PIXEL_UNPACK_BUFFER);
    mGL_GET_ERROR();
    glBindTexture( GL_TEXTURE_2D, texture);
    mGL_GET_ERROR();
    glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, cPICTURE_WIDTH, cPICTURE_HEIGHT,
        format, GL_UNSIGNED_BYTE, (void *) 0);
    mGL_GET_ERROR();
    glBindBuffer( GL_PIXEL_UNPACK_BUFFER, 0);
    mGL_GET_ERROR();
}*/

/*
 * Create a OpenGL texture which is available from memory (The raw Bayer 8-bit image)
 */
/*static GLuint make_texture(void * make_texture, GLenum format)
{
    GLuint texture;
    mGL_GET_ERROR();

    if (!(format == GL_RGBA8 || format == GL_LUMINANCE8))
	return 0; //we don't manage other texture format	

    glGenTextures(1, &texture);
    mGL_GET_ERROR();

    glBindTexture(GL_TEXTURE_2D, texture);
    mGL_GET_ERROR();

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    mGL_GET_ERROR();

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    mGL_GET_ERROR();

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,     GL_CLAMP_TO_EDGE);
    mGL_GET_ERROR();

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,     GL_CLAMP_TO_EDGE);
    mGL_GET_ERROR();

    glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP,    GL_FALSE);
    mGL_GET_ERROR();
	
    //data is bayer cfa
    if (format ==  GL_LUMINANCE8)
        glTexImage2D(
            GL_TEXTURE_2D, 0,           
            GL_LUMINANCE8,                    
            cPICTURE_WIDTH, cPICTURE_HEIGHT, 0,           
            GL_LUMINANCE, GL_UNSIGNED_BYTE,  
            image_ptr                
        );
    else if (format == GL_RGBA8 )
        glTexImage2D(
            GL_TEXTURE_2D, 0, 
            GL_RGBA8,   
            cPICTURE_WIDTH, cPICTURE_HEIGHT, 0,
            GL_RGBA, GL_UNSIGNED_BYTE,
            image_ptr
        );
    mGL_GET_ERROR();

    //Unbind
    glBindTexture(GL_TEXTURE_2D, 0);

    mGL_GET_ERROR();
    return texture;
}


//input and output is the argument, gives an RGBA picture
#ifdef GL_SAVE_NON_COMPRESSED_DEMOSAICED_PICTURE
//The RGBAPicture should already be malloced to the good size before being passed
int demosaiceBayer(void * bayerPicture, void * RGBAPicture )
#else
int demosaiceBayer(void * bayerPicture)
#endif
{
	mGL_GET_ERROR();

    glUseProgram(g_resources.program);

    mGL_GET_ERROR();

    //bind to framebufferObject
    glBindFramebuffer(GL_FRAMEBUFFER, g_resources.framebufferObject);

    mGL_GET_ERROR();

    //Set the constant uniforms
    glUniform4f(g_resources.uniforms.sourceSize, 
	(float) cPICTURE_WIDTH,
	(float) cPICTURE_HEIGHT,
	(float) 1/cPICTURE_WIDTH,
	(float) 1/cPICTURE_HEIGHT
    );
    mGL_GET_ERROR();

    glUniform2f(g_resources.uniforms.firstRed, 0.0f , 1.0f);
    mGL_GET_ERROR();
   
    //wait for signal to start rendering
    //TODO
    
    //Bind the attributes
    glBindBuffer(GL_ARRAY_BUFFER, g_resources.pixelCoordBuffer);
    mGL_GET_ERROR();

    glVertexAttribPointer(
	g_resources.attributes.pixelCoord, //attribute
	4,				   //size
	GL_FLOAT,		           //type
	GL_FALSE,			   //normalized
	sizeof(GLfloat)*4,		   // stride
	(void *) 0			   // offset
    );
    mGL_GET_ERROR();

    glEnableVertexAttribArray(g_resources.attributes.pixelCoord);
    mGL_GET_ERROR();

    
    //Load textures
    if( g_renderStage == 0 )
    {
	//select which buffer to draw to
	glDrawBuffer(GL_COLOR_ATTACHMENT0);
	glReadBuffer(GL_COLOR_ATTACHMENT0);
	mGL_GET_ERROR();

	glActiveTexture(GL_TEXTURE0);
	mGL_GET_ERROR();
	
	update_PBOtexture(g_resources.pixelBufferObject[0], g_resources.textureBayerPict[0],
			 GL_LUMINANCE, bayerPicture, cPICTURE_BYTE_SIZE);

	mGL_GET_ERROR();

	glUniform1i(g_resources.uniforms.textureBayerPict, 0);
	mGL_GET_ERROR();

    }
    else if(g_renderStage == 1)
    {
	//select which buffer to draw to
	glDrawBuffer(GL_COLOR_ATTACHMENT1);
	glReadBuffer(GL_COLOR_ATTACHMENT1);
	
	glActiveTexture(GL_TEXTURE1);
	
	update_PBOtexture(g_resources.pixelBufferObject[1], g_resources.textureBayerPict[1],
			  GL_LUMINANCE, bayerPicture, cPICTURE_BYTE_SIZE);
	glUniform1i(g_resources.uniforms.textureBayerPict, 1);
    }
    else
    	return -1; //error
	
    //Render
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, g_resources.elementBuffer);
    mGL_GET_ERROR();

    glDrawElements(
	GL_POINTS,			// mode
	cPICTURE_WIDTH*cPICTURE_WIDTH,	// count
	GL_UNSIGNED_INT,		// Type
	(void*) 0			// buffer offset
    );
    mGL_GET_ERROR();
	
    //Clean up 
    glDisableVertexAttribArray(g_resources.attributes.pixelCoord);
    mGL_GET_ERROR();
    
    //switch the render stage
    g_renderStage = (g_renderStage+1)%2;
	
#ifdef GL_SAVE_NON_COMPRESSED_DEMOSAICED_PICTURE
    //ReadPixels should return immediately
    glReadPixels(0,0, cPICTURE_WIDTH, cPICTURE_HEIGHT, GL_RGBA, GL_UNSIGNED_BYTE, RGBAPicture);
    mGL_GET_ERROR();

#endif
	
    return 0;
}*/

/*
 * Entry point
 */
/*int initializeDebayer(void)
{
    //fill constants
    
    for( int j = 0 ; j < cPICTURE_HEIGHT ; j++)
    {   
		for( int i = 0 ; i < cPICTURE_WIDTH ; i++)
		{
			g_pixelCoordBufferData[ ((j * cPICTURE_WIDTH + i)<<2) + 0] = (GLfloat) i/(cPICTURE_WIDTH)*2 -1.0f;
			g_pixelCoordBufferData[ ((j * cPICTURE_WIDTH + i)<<2) + 1] = (GLfloat) j/(cPICTURE_HEIGHT)*2 -1.0f;
			g_pixelCoordBufferData[ ((j * cPICTURE_WIDTH + i)<<2) + 2] = (GLfloat) i/(cPICTURE_WIDTH);
			g_pixelCoordBufferData[ ((j * cPICTURE_WIDTH + i)<<2) + 3] = (GLfloat) j/(cPICTURE_HEIGHT);
		}
    }

    for( int i =0 ; i < cPICTURE_HEIGHT * cPICTURE_WIDTH ; i++)
	g_elementBufferData[i] = (GLuint) i;
	
	//Initialize OpenGL
	intializeOpenGLContext();
	
    if( ogl_LoadFunctions() == ogl_LOAD_FAILED )
    {
        fprintf(stderr, "OpenGL 3.0 not available\n");
        return 1;
    }

    mGL_GET_ERROR();

    if (!make_resources()) {
        fprintf(stderr, "Failed to load resources\n");
        return 2;
    }
    mGL_GET_ERROR();
    return 0;
}

int destroyDebayer(void)
{
	destroyOpenGLContext();
	return 0;
}*/
