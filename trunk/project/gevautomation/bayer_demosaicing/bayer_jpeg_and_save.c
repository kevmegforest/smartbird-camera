/*
 * bayer_jpeg_and_save_processor.c
 *
 *  Created on: Jun 16, 2014
 *      Author: kevmegforest
 */


#include <unistd.h>
#include "bayer_demosaicing.h"
#include "../network_sender/JPEGStreamerClient.h"
#include "../CameraPicturesSpec.h"
#include "../ImageSavingUtilities.h"

#include <pthread.h>
#include <turbojpeg.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "../main.h"
#include <GL/freeglut.h>

//flow typique -> initialize(), add_...(), ..., cleanup();

static pthread_t threadId;
typedef enum
{
	RUN,
	STOP,
	FINISH
} tThreadStatus ;
static tThreadStatus threadStatus = FINISH;
static pthread_mutex_t mutexThreadStatus ;



static uint32_t stack_head = 0, stack_tail = 0;
static pthread_mutex_t mutexStackHead;
static pthread_mutex_t mutexStackTail;
static pthread_mutex_t mutexAddPicture;


typedef struct {
	uint8_t hour;
	uint8_t minute;
	uint8_t second;
	uint16_t millisecond;
	char rawBayerPictureData[cPICTURE_BYTE_SIZE];
} tRawBayerPictureAndInfo;

//have a stack of 16 pictures
#define cBAYER_JPEG_AND_SAVE_STACK_SIZE 16
//for rawBayerPictures
//tRawBayerPictureAndInfo stack_bayerPictures[cBAYER_JPEG_AND_SAVE_STACK_SIZE];
tRawBayerPictureAndInfo * stack_bayerPictures[cBAYER_JPEG_AND_SAVE_STACK_SIZE];
//for rgbaPictures
//unsigned char stack_rgbaPictureData[cBAYER_JPEG_AND_SAVE_STACK_SIZE][cPICTURE_HEIGHT * cPICTURE_WIDTH * 4];
unsigned char * stack_rgbaPictureData[cBAYER_JPEG_AND_SAVE_STACK_SIZE];


void * bayerJpegSave_thread(void * threadStatus )
{

	glutInit(&g_main_argc, g_main_argv);
	glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB);
	glutInitWindowSize (250, 250);
	glutInitWindowPosition (0, 0);
	glutCreateWindow ("awesome window");

	//initialise OpenGL
	initializeGL();

	//initialize stack
	pthread_mutex_lock(&mutexStackHead);
	pthread_mutex_lock (&mutexStackTail);
	stack_head = 0;
	stack_tail = 0;
	pthread_mutex_unlock (&mutexStackHead);
	pthread_mutex_unlock (&mutexStackTail);


	pthread_mutex_lock (&mutexThreadStatus);
	while( *( tThreadStatus * )threadStatus == RUN )
	{
		pthread_mutex_unlock(&mutexThreadStatus);

		pthread_mutex_lock(&mutexStackHead);
		pthread_mutex_lock(&mutexStackTail);
		if( stack_head != stack_tail )
		{
			//printf("\nnT_sendPacket...%p", (stack_packet[stack_head]) );
			pthread_mutex_unlock(&mutexStackTail);
			//get pointer to picture and it's data
			tRawBayerPictureAndInfo * bayerPtr = (stack_bayerPictures[stack_head]);
			unsigned char * rgbaPtr = (stack_rgbaPictureData[stack_head]);
			pthread_mutex_unlock(&mutexStackHead);

			//1-> Save Raw Bayer Picture
			//TODO Recreate function to use what we have instead
			SaveImageFileToDNG(cPICTURE_FORMAT,cPICTURE_WIDTH, cPICTURE_HEIGHT, cPICTURE_PIXEL_BIT_DEPTH, bayerPtr->hour, bayerPtr->minute, bayerPtr->second, bayerPtr->millisecond , bayerPtr->rawBayerPictureData);

			//2-> Debayer
			demosaiceBayer((bayerPtr->rawBayerPictureData)-1 , rgbaPtr );

			//4-> JPEG compression
			const int JPEG_QUALITY = 75;
			//const int COLOR_COMPONENTS = 3;
			long unsigned int _jpegSize = 0;
			unsigned char* _compressedImage = NULL; //!< Memory is allocated by tjCompress2 if _jpegSize == 0

			tjhandle _jpegCompressor = tjInitCompress();

			tjCompress2(_jpegCompressor, rgbaPtr, cPICTURE_WIDTH, 0, cPICTURE_HEIGHT, TJPF_RGBA,
			          &_compressedImage, &_jpegSize, TJSAMP_444, JPEG_QUALITY,
			          TJFLAG_ACCURATEDCT);

			tjDestroy(_jpegCompressor);

			//3.5
			SaveJPEGImageFile(cPICTURE_FORMAT,cPICTURE_WIDTH, cPICTURE_HEIGHT, 8,bayerPtr->hour,bayerPtr->minute,bayerPtr->second,bayerPtr->millisecond, _compressedImage , _jpegSize * sizeof(char));

			//4-> Send Picture to base station
			netStreamer_addPictureToSend(_compressedImage, _jpegSize * sizeof(char),bayerPtr->hour,bayerPtr->minute,bayerPtr->second,bayerPtr->millisecond);

			//3-> Free memory of _compressedImage
			tjFree(_compressedImage);


			pthread_mutex_lock(&mutexStackHead);
			//increase head
			stack_head = (stack_head + 1)%cBAYER_JPEG_AND_SAVE_STACK_SIZE;
			pthread_mutex_unlock (&mutexStackHead);
		}
		else
		{
			pthread_mutex_unlock (&mutexStackHead);
			pthread_mutex_unlock (&mutexStackTail);
		}
		usleep(1000);
		pthread_mutex_lock (&mutexThreadStatus);
	}
	cleanupGL();
	*( tThreadStatus * )threadStatus = FINISH;
	pthread_mutex_unlock (&mutexThreadStatus);

	pthread_exit(NULL);
}

void bayerJpegSave_addRawBayerPicture(void * f_pictureData, uint8_t f_hour, uint8_t f_minute, uint8_t f_second, uint16_t f_millisecond)
{
	pthread_mutex_lock(&mutexAddPicture);

	//printf("\naPS_beginPictureAdd");
	pthread_mutex_lock(&mutexStackTail);
	pthread_mutex_lock(&mutexStackHead);
	if((stack_tail + 1)%cBAYER_JPEG_AND_SAVE_STACK_SIZE == stack_head)
	{
		printf("\n*** [ERROR] bayerJpegSave Stack is Full : \nFreeing memory to send more photos! \n");
		stack_tail = (stack_head + 2)%cBAYER_JPEG_AND_SAVE_STACK_SIZE;
		int end_pos = stack_head;
		int pos = stack_tail;
		while (end_pos != pos)
		{
			pos = (pos+1)%cBAYER_JPEG_AND_SAVE_STACK_SIZE;
		}
	}
	//get Pointer to structure
	tRawBayerPictureAndInfo * ptr = stack_bayerPictures[stack_tail];

	pthread_mutex_unlock(&mutexStackTail);
	pthread_mutex_unlock(&mutexStackHead);
	//printf("\naPS_pointer = %p", pPacket);
	//Create header
	ptr->hour=f_hour;
	ptr->minute=f_minute;
	ptr->second=f_second;
	ptr->millisecond=f_millisecond;
	//memcopy the picture
	memcpy( ptr->rawBayerPictureData , f_pictureData  , cPICTURE_BYTE_SIZE );
	//add pointer to stack
	pthread_mutex_lock(&mutexStackTail);
	//increase stack_tail
	stack_tail = (stack_tail + 1)%cBAYER_JPEG_AND_SAVE_STACK_SIZE;
	pthread_mutex_unlock(&mutexStackTail);

	//printf("\naPS_end");
	pthread_mutex_unlock(&mutexAddPicture);
}

void bayerJpegSave_initialize()
{
	pthread_mutex_init(&mutexStackHead, NULL);
	pthread_mutex_init(&mutexStackTail, NULL);
	pthread_mutex_init(&mutexThreadStatus, NULL);
	pthread_mutex_init(&mutexAddPicture, NULL);

	//allocate memory for stack
	for(int i=0;i< cBAYER_JPEG_AND_SAVE_STACK_SIZE ; i++)
	{
		stack_bayerPictures[i]= malloc(sizeof(tRawBayerPictureAndInfo));
		stack_rgbaPictureData[i]=malloc(cPICTURE_HEIGHT * cPICTURE_WIDTH * 4*sizeof(char));
	}
	//initializeNetwork
	netStreamer_initialize();

	//create thread that will contains image pointer and then send those pictures
	pthread_mutex_lock(&mutexThreadStatus);
	if(threadStatus == FINISH)
	{
		threadStatus = RUN;
		pthread_create(&threadId,NULL, bayerJpegSave_thread , (void *) &threadStatus);
	}
	pthread_mutex_unlock(&mutexThreadStatus);
}

void bayerJpegSave_cleanup()
{
	usleep(100000);
	printf("\nCleanup \n");
	pthread_mutex_lock(&mutexThreadStatus);
	threadStatus=STOP;
	while(threadStatus!=FINISH)
	{
		pthread_mutex_unlock(&mutexThreadStatus);
		usleep(10000);
		pthread_mutex_lock(&mutexThreadStatus);
	}
	pthread_mutex_unlock(&mutexThreadStatus);

	pthread_mutex_destroy(&mutexStackHead);
	pthread_mutex_destroy(&mutexStackTail);
	pthread_mutex_destroy(&mutexThreadStatus);
	pthread_mutex_destroy(&mutexAddPicture);
	netStreamer_cleanup();

	for(int i=0;i< cBAYER_JPEG_AND_SAVE_STACK_SIZE ; i++)
	{
		free(stack_bayerPictures[i]);
		free(stack_rgbaPictureData[i]);
	}
}

