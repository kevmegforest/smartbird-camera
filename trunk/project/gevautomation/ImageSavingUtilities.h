#ifndef __ImageSavingUtilities_H_
#define __ImageSavingUtilities_H_

#include <stdint.h>
#include <time.h>

int SaveRGBAImageFileToDNG(uint32_t format, uint32_t width, uint32_t height, uint32_t depth, uint32_t timestamp_hi,uint32_t timestamp_lo, time_t initial_time, void * image_ptr );

//Save a file according to the DNG 1.4 spec. This is for raw pictures
int SaveImageFileToDNG(uint32_t format, uint32_t width, uint32_t height, uint32_t depth, uint32_t f_hour, uint32_t f_min, uint32_t f_sec, uint32_t f_msec  , void * image_ptr );

int SaveImageFileToBitMap( uint32_t format, uint32_t width, uint32_t height, uint8_t depth, uint32_t timestamp_hi,uint32_t timestamp_lo, void * image_ptr );

int SaveJPEGImageFile(uint32_t format, uint32_t width, uint32_t height, uint32_t depth, uint32_t f_hour, uint32_t f_min, uint32_t f_sec, uint32_t f_msec  , void * image_ptr, uint32_t f_fileSize );

#endif /* __ImageSavingUtilities_H_ */

