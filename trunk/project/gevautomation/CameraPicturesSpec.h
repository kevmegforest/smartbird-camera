#ifndef __CameraPicturesSpec_H_
#define __CameraPicturesSpec_H_

#define cPICTURE_WIDTH                  (4096)
#define cPICTURE_HEIGHT                 (3072)
#define cPICTURE_PIXEL_BIT_DEPTH        (8)
#define cPICTURE_PIXEL_BYTE_SIZE        (1)
#define cPICTURE_FRAMERATE              (8000) //in mHz
#define cPICTURE_BYTE_SIZE              (cPICTURE_WIDTH * cPICTURE_HEIGHT * cPICTURE_PIXEL_BYTE_SIZE) 
#define cPICTURE_FORMAT                 (0x0108000A) //BayerGB8

#endif /* __CameraPicturesSpec_H_ */

