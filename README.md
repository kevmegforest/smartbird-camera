###########################################################
#Équipe Smartbird - École Polytechnique de Montréal       #
#---------------------------------------------------------#
#Logiciel de démosaiquage et de compression JPEG embarqué #
###########################################################

# Page de la société technique 	
#	http://smartuav.aep.polymtl.ca/

# Wiki & Documentation 
#	Smartbird: http://smartuav.aep.polymtl.ca/w/index.php?title=Accueil
#	Équipe informatique: http://smartuav.aep.polymtl.ca/w/index.php?title=%C3%89quipe_informatique